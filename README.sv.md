Detta är mina konfigurationsfiler. Uppströms ska vara
https://git.lysator.liu.se/jaadu/dotfiles. Standardinställningar är:

- WM: i3 med xfce4-panel
- Shell: Bash (men Fish används som skal till tangentbordsgenvägen i
  stumpwm)
- Text-editor: Vim, men Emacs är välkonfigurerat

Installation
============

Skapa först en ssh-nyckel med:

```
ssh-keygen -t ed25519 -a 100
```

Klona sedan detta repo.

Kör sedan `make` för att skapa symlänkar i `$HOME` och
`$HOME/.config`. Strukturen i detta repo är inspirerad av GNU Stow,
men använder sig inte av det.

Konfigurationsfiler
====================

`home` och `config` symlänkas till `$HOME` respektive `$HOME/.config`.
Makefilen ser till att de filer som kommer skrivas över, så görs en
backup först. Mappen guix är scheme filer tänkt för att köras till
guix.

rc-filer
--------------------

De flesta distributioner laddar start filer i denna ordningen:

| Fil         | Körs...                | Kör även... |
|-------------+------------------------+-------------|
| `.profile`  | Under varje inloggning |             |
| `.bashrc`   | interaktivt skal       |             |
| `.xinitrc`  |                        |             |
| `.xsession` | Av startx              | `.xinitrc`  |

Installera Guix
---------------



Först, stäng av SELinux om det är påslaget. Redigera filen
/etc/selinux/config. Guix har definitioner, men de funkar inte.

Se: `(info "(guix) Binary Installation")`

Installera guix-programmen med `guix package
--manifest=$HOME/dotfiles/guix/home-config/home-packages.scm`.

Konfigurera X
-------------

Fedora kan köra .xsession med paketet `xorg-x11-xinit-session`.

Emacs
-----

Starta Emacs och kör `M-x package-install-selected-packages`.

E-post
------

Kör `$SYNCDIR/setup.sh mail`.

Mer Setup
=========

Saker som kan behöva fixas för att allt ska funka.

## Systemet

### Systemd användning

För att starta en systemd-tjänst kör följande:

```
systemctl --user enable --now <Tjänst>
```

### Hostnamn

Redigera filen `/etc/hostname`. Uppdatering sker vid nästa omstart.

## Användargränsnitt

### Ändra tangentbordslayout

```
localectl set-x11-keymap se pc105 nodeadkeys caps:ctrl_modifier
```

### Aktivera fingeravtrycksläsare

Använd `fprintd-enroll`.

## Windows

Detta använder [msys2](https://www.msys2.org/) för att skapa en linux-miljö
på Windows. För att köra igång, gör nedanstående:

- Lägg till miljövariabeln ´%HOME%´ (Sök i startmenyn).
- Installera msys2.
  - Lägg till ´msys2/bin´ till path.
  - `pacman -syu` för att uppdatera.
  - För att msys2 ska ha tillgång till Windows path, lägg till
    miljövariabeln ´MSYS2_PATH_TYPE´ med värdet ´inherit´.

## VIM

Använd git submodules för att installera minpac vilket sedan
installerar med hjälp av ´PackInstall´ plugins.

## Emacs

Ingen mer setup krävs för emacs, men det kan vara bra att bygga från
källkoden för att få senaste versionen. För att bygga Emacs gör följande:

1. Ladda ner emacs från [hemsidan](https://www.gnu.org/software/emacs/download.html#gnu-linux)
   och packa upp.
2. Bygg Emacs med `./configure --prefix=${HOME}/.local` och sedan
   `make`.
3. Och kanske `make install`.

## Firefox

Gå till inställningar -> Allmänt, Vid Start, Öppna föregående fönster och
flikar

Några viktiga plugins:

| Plugin                     | Beskrivning                                |
|----------------------------+--------------------------------------------|
| Vimium-C                   | Vim bindings, bättre defaults än Vim-vixen |
| Ublock Origin              | adblocker                                  |
| Privacy Badger             | EFFs plugin                                |

## Syncthing

Starta med: `systemctl --user enable --now syncthing.service`. Sedan
kan gränssnittet kommas åt via http://localhost:8384/.

## Latex

Mycket borde finnas, vissa mindre vanliga paket som är bra att ha
eller som används av org-mode-export. Dessa måste hämtas vid sidan av.

Dessa är:

```
wrapfig capt-of
```
