Använder Pandoc för att skapa .pdf och .html filer

I.   ANVÄNDNING

make
make pdfs
make html

För att redigera templates till t.ex html använd kommandot:

pandoc -D html > template.html

II.  BEROENDEN

- Pandoc
- make
- Latex (För pdf)
- pandoc-citeref (för citat, ta bort ur makefile ifall det inte behövs)


III. DOKUMENTATION

Pandoc 		http://pandoc.org/MANUAL.html
