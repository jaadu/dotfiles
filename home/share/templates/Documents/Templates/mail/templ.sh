#!/bin/sh
# 
# Needs four arguments
# 1. mailbox name
# 2. imap name
# 3. smtp name
# 4. the users email
# 5. the users name

# TODO $0 is not a reliable way of getting script dir

set -e

if [ -z "$5" ]; then
    echo "Du behöver 5 argument" >&2 exit 1;
fi

# %MAILBOX-NAME%
# %IMAP-SERVER%
# %SMTP-SERVER%
# %USER_EMAIL%
# %USER_NAME%

MBSYNC_TEMPLATE="$(dirname "$0")/mbsyncrc-template"
MBSYNC_OUT="$HOME/.mbsyncrc"
MSMTP_BASE="$(dirname "$0")/msmtprc-base"
MSMTP_TEMPLATE="$(dirname "$0")/msmtprc-template"
MSMTP_OUT="$HOME/.msmtprc"

echo "Skriver till $MBSYNC_OUT" >&2
sed -e "s/%MAILBOX_NAME%/$1/g" \
    -e "s/%IMAP_SERVER%/$2/g" \
    -e "s/%USER_EMAIL%/$4/g" \
    "$MBSYNC_TEMPLATE" >> "$MBSYNC_OUT"

echo "Skriver till $MSMTP_OUT" >&2

if [ ! -f "$MSMTP_OUT" ]; then
    sed -e "s/%MAILBOX_NAME%/$1/g" \
        "$MSMTP_BASE" > "$MSMTP_OUT";
fi

sed -e "s/%MAILBOX_NAME%/$1/g" \
    -e "s/%SMTP_SERVER%/$3/g" \
    -e "s/%USER_EMAIL%/$4/g" \
    -e "s/%USER_FROM%/$5/g" \
    "$MSMTP_TEMPLATE" >> "$MSMTP_OUT"

mkdir -p "$HOME/Mail/$1"

chmod 600 "$HOME/.msmtprc"            # Contains secrets

echo "klart!" >&2
echo "Sätt lösenord i ~./.netrc" >&2
