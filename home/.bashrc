# Interactive shell config
# If not running interactively, don't do anything
[[ $- == *i* ]] || return

# Enable completion, must run BEFORE /etc/bashrc
[ -f "$HOME/.guix-profile/etc/profile.d/bash_completion.sh" ] && \
    . "$HOME/.guix-profile/etc/profile.d/bash_completion.sh"

# Machine settings
[ -f /etc/bashrc ] && . /etc/bashrc

# 

# History

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL="ignoreboth:erasedups"
HISTTIMEFORMAT="%F %T "
# append to the history file, don't overwrite it
shopt -s histappend

HISTFILE="$HOME/.config/bash_history"

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# 

# Convenience

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern '**' used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# 

# Prompt

red="\e[31m"
green="\e[32m"
blue="\e[34m"
magenta="\e[35m"
reset="\e[0m"

# Window Title
if [[ "$TERM" == xterm* ]]; then
    PS0='\e]2;\u@\h: $(fc -nl -0 | xargs) \007'
fi

# Last commands exit code
PS1='$([ $? -ne 0 ] && printf ${red}[$?]${reset})\n'

# Set title again to make sure terminel doesn't override it
if [[ "$TERM" == xterm* ]]; then
    PS1+='\[\e]2;\u@\h: \w\007\]'
fi

# GUIX shell environment
if [ "$GUIX_ENVIRONMENT" ]; then
    PS1+="${magenta}$GUIX_ENVIRONMENT${reset}\n"
fi

PS1+="${green}\u@${reset}\h ${blue}\w${reset}\n"
PS1+="\$ "

export PS2="${red}>${reset} "

# 

# Local settings

# Home specific config
[ -f "$HOME/.bashrc_local" ] && . "$HOME/.bashrc_local"

# 

# Alias

alias ls="ls --color=auto"
alias grep="grep --color=auto"

# 

# Custom Functions

genpass() {
    tr -dc '_#$%&=+?A-HJ-Za-hkm-z02-9-' < /dev/urandom | head -c"${1:-10}"
    echo ""
}

tldr() {
    curl -s "https://raw.githubusercontent.com/tldr-pages/tldr/master/pages/common/$1.md"
}

colors () {
    printf "All The colors:\n"
    printf "\n"
    printf "\e[30m color0       (black)\n"
    printf "\e[31m color1       (red)\n"
    printf "\e[32m color2       (green)\n"
    printf "\e[33m color3       (yellow)\n"
    printf "\e[34m color4       (blue)\n"
    printf "\e[35m color5       (magenta)\n"
    printf "\e[36m color6       (cyan)\n"
    printf "\e[37m color7       (white)\n"
    printf "\e[0m"
    printf "\e[90m color8       (bright black)\n"
    printf "\e[91m color9       (bright red)\n"
    printf "\e[92m color10      (bright green)\n"
    printf "\e[93m color11      (bright yellow)\n"
    printf "\e[94m color12      (bright blue)\n"
    printf "\e[95m color13      (bright magenta)\n"
    printf "\e[96m color14      (bright cyan)\n"
    printf "\e[97m color15      (bright white)\n"
    printf "\e[0m"
}
