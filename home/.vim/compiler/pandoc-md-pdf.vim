" from https://github.com/vim-pandoc/vim-pandoc.git
if exists("current_compiler")
  finish
endif

let current_compiler = "pandoc-md-pdf"

CompilerSet errorformat="%f",\ line\ %l:\ %m
CompilerSet makeprg=pandoc\ %\ -f\ markdown\ -t\ latex\ -s\ -o\ %:r.pdf
