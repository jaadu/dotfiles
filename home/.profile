# Make sure not to double-source
test "${DOTFILE_DIR}" && return 0

export EDITOR=vim
export PAGER=less
export MAILDIR="${HOME}/Maildir"
export SYNCDIR="${HOME}/Sync"
export LANG=sv_SE.UTF-8

export DOTFILE_DIR="${HOME}/dotfiles"

# set PATH so it includes user's private bin directories
export PATH="\
${HOME}/bin:\
${HOME}/.local/bin:\
${PATH}"

# 

export GUILE_LOAD_PATH="${GUILE_LOAD_PATH}${GUILE_LOAD_PATH:+:}${DOTFILE_DIR}/guix"

# Guix Vars
if command -v guix >/dev/null
then
    export MANPATH="/usr/share/man${MANPATH:+:}${MANPATH}"
    export GUIX_PACKAGE_PATH="${DOTFILE_DIR}/guix/packages/"
    for GUIX_PROFILE in "$HOME/.config/guix/current" "$HOME/.guix-profile" ; do
        . "$GUIX_PROFILE/etc/profile"
        # Mbsync needs certificates provided by the nss-certs package
        [ -d "$GUIX_PROFILE/etc/ssl/certs" ] && export SSL_CERT_DIR="$GUIX_PROFILE/etc/ssl/certs"
    done
fi

# 

# Additional npm-setup
NPM_PACKAGES="${HOME}/.npm-packages"

export PATH="$PATH:$NPM_PACKAGES/bin"

# Preserve MANPATH if you already defined it somewhere in your config.
# Otherwise, fall back to `manpath` so we can inherit from `/etc/manpath`.
export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"

# 

# Stop GTK apps from complaining. GTK thinks it needs usability plugins like
# screenreaders.
export NO_AT_BRIDGE=1

[ -e .profile.local.sh ] && . .profile.local.sh

# 

# Om X.org är igång
if [ "$DISPLAY" ]; then
    # Stäng av pling
    xset b off &

    # Turn on numlock
    numlockx on &

    # Config for X resource database
    xrdb "$HOME"/.Xresources
    [ -f "$SYNCDIR"/host-config/"$(hostname)"-xresources ] && xrdb -merge "$SYNCDIR"/host-config/"$(hostname)"-xresources

    # Justera tangentbordslayout
    setxkbmap -variant nodeadkeys -option caps:ctrl_modifier

    # Set fonts
    xset +fp "$(dirname "$(readlink -f ~/.guix-profile/share/fonts/truetype/fonts.dir)")"
else
    if [ "$(id | grep -q 'loadkeys')" ] ; then
        loadkeys se-nodeadkeys
        loadkeys ~/share/caps-ctrl.map
    fi
fi

# 

[ -n "$BASH" ] && [ -r ~/.bashrc ] && . ~/.bashrc
