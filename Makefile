.PHONY: links update-guix clean

HOMEFILES:=$(filter-out home/.. home/.,$(wildcard home/*) $(wildcard home/.*))
CONFIGFILES:=$(wildcard config/*)

HOMETARGETS:=$(patsubst home/%,~/%,$(HOMEFILES))
CONFIGTARGETS:=$(patsubst config/%,~/.config/%,$(CONFIGFILES))

links: | $(CONFIGTARGETS) $(HOMETARGETS)

~/%: $(PWD)/home/%
	if [ -e $@ ] && ! [ -L $@ ] ; then \
	    mv $< $<.bak ; \
	    mv $@ $< ; \
	fi
	ln -sf ${<:$(HOME)/%=%} $@

~/.config/%: $(PWD)/config/%
	if [ -e $@ ] && ! [ -L $@ ] ; then \
	    mv $< $<.bak ; \
	    mv $@ $< ; \
	fi
	ln -sf ${<:$(HOME)/%=../%} $@

update-guix: guix/home-config/home-packages.scm
	guix pull
	guix package -m $<

clean:
	rm $(CONFIGTARGETS) $(HOMETARGETS)
