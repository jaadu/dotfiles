(in-package :stumpwm-user)

(set-prefix-key (kbd "s-t"))

(define-key *top-map* (kbd "s-RET") "exec")
(define-key *top-map* (kbd "s-S-RET") "exec urxvt -e fish")

(prog1 'window-moving
  (define-key *top-map* (kbd "s-h") "move-focus left")
  (define-key *top-map* (kbd "s-j") "move-focus down")
  (define-key *top-map* (kbd "s-k") "move-focus up")
  (define-key *top-map* (kbd "s-l") "move-focus right")
  (define-key *top-map* (kbd "s-H") "move-window left")
  (define-key *top-map* (kbd "s-J") "move-window down")
  (define-key *top-map* (kbd "s-K") "move-window up")
  (define-key *top-map* (kbd "s-L") "move-window right"))

;; (prog1 'dynamic-group
;;   (define-key *top-map* (kbd "s-n") "rotate-windows forward")
;;   (define-key *top-map* (kbd "s-p") "rotate-windows backward"))

(define-key *top-map* (kbd "s-1") "gselect 1")
(define-key *top-map* (kbd "s-2") "gselect 2")
(define-key *top-map* (kbd "s-3") "gselect 3")
(define-key *top-map* (kbd "s-4") "gselect 4")
(define-key *top-map* (kbd "s-5") "gselect 5")
(define-key *top-map* (kbd "s-6") "gselect 6")
(define-key *top-map* (kbd "s-7") "gselect 7")
(define-key *top-map* (kbd "s-8") "gselect 8")
(define-key *top-map* (kbd "s-9") "gselect 9")
(define-key *top-map* (kbd "s-0") "gselect 10")

(define-key *top-map* (kbd "s-S-1") "gmove 1")
(define-key *top-map* (kbd "s-S-2") "gmove 2")
(define-key *top-map* (kbd "s-S-3") "gmove 3")
(define-key *top-map* (kbd "s-S-4") "gmove 4")
(define-key *top-map* (kbd "s-S-5") "gmove 5")
(define-key *top-map* (kbd "s-S-6") "gmove 6")
(define-key *top-map* (kbd "s-S-7") "gmove 7")
(define-key *top-map* (kbd "s-S-8") "gmove 8")
(define-key *top-map* (kbd "s-S-9") "gmove 9")
;(define-key *top-map* (kbd "s-S-0") "gmove 10")

(define-key *top-map* (kbd "s-!") "gmove 1")
(define-key *top-map* (kbd "s-\"") "gmove 2")
(define-key *top-map* (kbd "s-#") "gmove 3")
(define-key *top-map* (kbd "s-currency") "gmove 4")
(define-key *top-map* (kbd "s-%") "gmove 5")
(define-key *top-map* (kbd "s-&") "gmove 6")
(define-key *top-map* (kbd "s-/") "gmove 7")
(define-key *top-map* (kbd "s-(") "gmove 8")
(define-key *top-map* (kbd "s-)") "gmove 9")
; (define-key *top-map* (kbd "s-=") "gmove 10") ; Used for `balance-frames' instead

(define-key *top-map* (kbd "s-TAB") "pull-hidden-next")
(define-key *top-map* (kbd "s-n")   "fnext")
(define-key *top-map* (kbd "s-p")   "fprev")

(define-key *top-map* (kbd "s-s") "vsplit")
(define-key *top-map* (kbd "s-v") "hsplit")
(define-key *top-map* (kbd "s-o") "only")
(define-key *top-map* (kbd "s-d") "remove-split")

(define-key *top-map* (kbd "s-+") "resize 10 0")
(define-key *top-map* (kbd "s--") "resize -10 0")
(define-key *top-map* (kbd "s-=") "balance-frames")

(define-key *top-map* (kbd "s-P") "exec systemctl suspend")
(define-key *top-map* (kbd "s-q") "delete") ; Kill focused window

;; Media keys are handled by panel plugin
;; (prog1 'audio
;;   (define-key *top-map* (kbd "XF86AudioLowerVolume") "exec amixer set Master 5%-")
;;   (define-key *top-map* (kbd "XF86AudioRaiseVolume") "exec amixer set Master 5%+")
;;   (define-key *top-map* (kbd "XF86AudioMute")        "exec amixer set Master toggle"))

(define-key *top-map* (kbd "s-;") "colon")
(define-key *top-map* (kbd "s-:") "eval")
(define-key *top-map* (kbd "s-g") "abort")

(setf *float-window-modifier* :super)

(define-key *input-map* (kbd "S-Insert") 'stumpwm::input-yank-clipboard)

