(require :ttf-fonts)

(setf xft:*font-dirs* (cons (concat (getenv "HOME") "/.guix-profile/share/fonts/") xft:*font-dirs*))
(setf clx-truetype:+font-cache-filename+ (concat (getenv "HOME") "/.local/share/fonts/font-cache"))
(xft:cache-fonts)
(stumpwm:set-font (make-instance 'xft:font :family "DejaVu Sans Mono" :subfamily "Book" :size 13))

