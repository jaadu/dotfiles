(use-modules (shepherd support))

(define syncthing
  (service
   '(syncthing)
   #:documentation "Syncthing - Open Source Continuous File Synchronization"
   #:start (make-forkexec-constructor
            '("syncthing" "serve" "--no-browser" "--no-restart" "--logflags=0"))
   #:stop (make-kill-destructor)
   #:respawn? #f))

(register-services (list syncthing))
