;; Add to your ~/.bash_profile:
;;
;; SSH_AUTH_SOCK=${XDG_RUNTIME_DIR-$HOME/.cache}/ssh-agent/socket
;; export SSH_AUTH_SOCK

(use-modules (shepherd support))

(define ssh-agent
  (service
   '(ssh-agent)
   #:documentation "Run `ssh-agent'"
   #:start (lambda ()
             (let ((socket-dir (string-append %user-runtime-dir "/ssh-agent")))
               (unless (file-exists? socket-dir)
                 (mkdir-p socket-dir)
                 (chmod socket-dir #o700))
               (fork+exec-command
                `("ssh-agent" "-D" "-a" ,(string-append socket-dir "/socket"))
                #:log-file (string-append %user-log-dir "/ssh-agent.log"))))
   #:stop (make-kill-destructor)
   #:respawn? #t))

(register-services (list ssh-agent))
