(use-modules (shepherd support))

(define pantalaimon
  (service
   '(pantalaimon)
   #:documentation "E2E Matrix reverse proxy"
   #:start (make-forkexec-constructor '("pantalaimon"))
   #:stop (make-kill-destructor)
   #:respawn? #f))

(register-services (list pantalaimon))
