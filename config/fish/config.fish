set -U fish_greeting

function fish_prompt
    set -l last_status $status
    if test $last_status -ne 0
         printf '%s[%s] ' \
        (set_color $fish_color_status) \
        $last_status
    end
    echo ""

    printf '%s%s@%s%s %s%s%s %s' \
    (set_color $fish_color_user) \
    (whoami) \
    \
    (set_color $fish_color_host) \
    (prompt_hostname) \
    \
    (set_color $fish_color_cwd) \
    (prompt_pwd) \
    (set_color normal) \
    \
    (fish_vcs_prompt)
    printf "\n\$ "
end

function fish_title
    if [ $_ = 'fish' ]
        echo fish (prompt_pwd)
    else
        echo $_
    end
end
