;; -*- lexical-binding: t; -*-
;; Abbreviations `(autotype)'.
;;
;; For alternatives see `skeleton' and `tempo'.
;;
;; See `tempel-path' for templates

(add-to-list 'package-selected-packages 'tempel)
(add-to-list 'package-selected-packages 'tempel-collection)
(add-to-list 'package-selected-packages 'eglot-tempel)

(with-eval-after-load 'tempel
  (define-key tempel-map (kbd "M-p") 'tempel-previous)
  (define-key tempel-map (kbd "M-n") 'tempel-next)
  (with-eval-after-load 'evil
    (define-key tempel-map (kbd "M-k") 'tempel-previous)
    (define-key tempel-map (kbd "M-j") 'tempel-next)
    (evil-make-intercept-map tempel-map 'insert)
    (add-hook 'evil-insert-state-exit-hook 'tempel-done)))

(setq tempel-path
  (list
   (expand-file-name "share/templates.eld" my-config-dir)
   (expand-file-name "share/local-templates.eld" my-config-dir)))
(add-to-list 'ensured-bookmark-alist `("templates.eld" ,(file-truename (car tempel-path))))

(when (locate-library "tempel")
  (define-key global-map (kbd "C-t") #'tempel-complete)
  (with-eval-after-load 'evil
    (define-key evil-insert-state-map (kbd "C-t") nil)))
(with-eval-after-load 'eglot
  (when (locate-library "eglot-tempel")
    (eglot-tempel-mode t)))

