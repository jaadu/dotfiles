;; -*- lexical-binding: t; -*-

;; Guix packages
(when (fboundp 'guix-emacs-autoload-packages)
  (guix-emacs-autoload-packages))



(defvar geiser-guix--process)
(defun geiser-guix ()
  "Launch a guix-repl and connect to it with geiser."
  (interactive)
  (with-environment-variables (("INSIDE_EMACS" "1"))
    (setq geiser-guix--process
          (start-process "guix repl" "*guix-repl*"
                         "guix" "repl" "--listen=tcp:37146")))
  ;; BUG geiser doesn't wait for the repl to start and errors on startup
  (message "You should probably run `M-x geiser-connect'")
  (set-process-sentinel geiser-guix--process 'geiser-guix-sentinel))

;; Bugged
(defun geiser-guix-sentinel (process event)
  (message "geiser-guix %s: %s" process event)
  (when (= 0 (process-exit-status process))
    (geiser-connect 'guile "localhost" 37146)))

