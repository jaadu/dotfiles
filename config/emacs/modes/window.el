;; -*- lexical-binding: t; -*-


;; (setq display-buffer-alist nil)
(setq display-buffer-alist
      ;; Display doc buffers in side windows
      '(;; ((or (major-mode . Info-mode)
        ;;      (major-mode . help-mode)
        ;;      (major-mode . helpful-mode)
        ;;      (major-mode . apropos-mode))
        ;;  (display-buffer-reuse-mode-window display-buffer-in-direction)
        ;;  (direction . right))
      ("\\*Calendar\\*"
       (display-buffer-reuse-mode-window display-buffer-below-selected)
       (dedicated . t)
       (window-height . fit-window-to-buffer))))

(define-minor-mode zoom-mode
  "Make sure that the selected window is always at least 80 chars wide"
  :global t
  :lighter " zoom"
  (defun set-width-to-decent (&rest _)
    (let ((delta (- 80 (window-width))))
      (when (> delta 0)
        (window-resize nil delta t))))
  (if zoom-mode
      (add-hook 'window-selection-change-functions #'set-width-to-decent)
    (remove-hook 'window-selection-change-functions #'set-width-to-decent)))



;;; Ace-window
;;
;; Jump between windows with a single stroke
(setq aw-scope 'frame
      aw-dispatch-always t
      aw-background nil)
(when (locate-library "ace-window")
  (with-eval-after-load 'evil
    (define-key 'evil-window-map "a" #'ace-window)))
(with-eval-after-load 'ace-window
  (set-face-attribute 'aw-mode-line-face nil :foreground "red" :weight 'bold :inherit nil))

