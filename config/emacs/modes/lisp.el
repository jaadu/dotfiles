;;; -*- lexical-binding: t; -*-

;;; Lisp
(setq inferior-lisp-program "sbcl")

(or (load "~/.guix-profile/share/common-lisp/sbcl/slynk/sly-autoloads.el" :noerror)
    (load "~/.guix-profile/share/common-lisp/source/cl-slynk/sly-autoloads.el" :noerror)
    (when-let ((file (car (file-expand-wildcards "~/.config/quicklisp/dists/quicklisp/software/sly-*/sly-autoloads.el"))))
      (load file :noerror))
    (message "sly not available"))
(setq-default sly-symbol-completion-mode nil) ; Otherwise Sly interferes with completion frameworks
   (with-eval-after-load 'sly
     (define-key sly-editing-mode-map (kbd "C-M-n") #'sly-next-note)
     (define-key sly-editing-mode-map (kbd "C-M-p") #'sly-previous-note)
     (with-eval-after-load 'evil
       (evil-define-key '(motion normal) sly-mode-map "K" #'sly-describe-symbol)
       (add-hook 'sly-mode-hook (defun evil-goto-sly-edit () (setq-local evil-goto-definition-functions 'sly-edit-definition)))
       (add-hook 'sly-popup-buffer-mode-hook #'evil-motion-state)))



;;; emacs-lisp
(with-eval-after-load 'evil
  (evil-define-key 'normal emacs-lisp-mode-map
    (kbd "C-m") 'eval-last-sexp)
  (add-hook 'edebug-mode-hook 'evil-normalize-keymaps))

