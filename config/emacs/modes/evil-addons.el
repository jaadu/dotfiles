;; -*- lexical-binding: t; -*-

;; Addons for evil-mode



;;; To use fd chord for escape

;; Use key-chord instead of evil escape, evil escape exits too much
;; Sadly this package also breaks sometimes
(add-to-list 'package-selected-packages 'key-chord)
(when (fboundp 'key-chord-mode)
  (key-chord-mode 1)
  ;; `key-chord-define' also automatically defines keychord key2, key1 so don't use that
  (define-key minibuffer-mode-map [key-chord ?f ?d] 'abort-minibuffers)
  (define-key isearch-mode-map [key-chord ?f ?d] 'isearch-abort) ; Broken
  (with-eval-after-load 'evil
    (define-key evil-normal-state-map [key-chord ?f ?d] 'evil-force-normal-state)
    (define-key evil-visual-state-map [key-chord ?f ?d] 'evil-exit-visual-state)
    (define-key evil-insert-state-map [key-chord ?f ?d] 'evil-normal-state)
    (define-key evil-replace-state-map [key-chord ?f ?d] 'evil-normal-state)
    (define-key evil-ex-completion-map [key-chord ?f ?d] 'abort-recursive-edit)
    (define-key evil-eval-map [key-chord ?f ?d] 'abort-recursive-edit)))



;;; evil-lion
(when (fboundp 'evil-lion-mode)
  (define-key evil-normal-state-map "gl" #'evil-lion-left)
  (define-key evil-normal-state-map "gL" #'evil-lion-right)
  (define-key evil-visual-state-map "gl" #'evil-lion-left)
  (define-key evil-visual-state-map "gL" #'evil-lion-right))
