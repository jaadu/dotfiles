;; -*- lexical-binding: t; -*-

(with-eval-after-load 'eglot
  (add-hook
   'eglot-managed-mode-hook
   (defun eglot-setup ()
     (when (eglot-managed-p)
       (setq-local evil-lookup-func #'eldoc-doc-buffer
                   evil-goto-definition-functions '(evil-goto-definition-xref)
                   eldoc-echo-area-use-multiline-p nil))))
  (with-eval-after-load 'evil
    (evil-define-key 'normal eglot-mode-map (kbd "g r") #'eglot-code-actions)
    (evil-define-key 'normal eglot-mode-map (kbd "g R") #'eglot-rename)))

