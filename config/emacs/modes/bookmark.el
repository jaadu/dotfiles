;; -*- lexical-binding: t; -*-

;;; Bookmarks
(define-key 'shortcut-map (kbd "p") #'bookmark-jump)
(setq bookmark-save-flag t)



(defun have-bookmark (bookmark file)
  "Make sure `bookmark' exist in bookmarks.

If `bookmark' doesn't exist, add it pointing to `file'."
  (cl-flet ((compare-fn (s1 s2) (equal (car s1) (car s2))))
    (add-to-list 'bookmark-alist
                 `(,bookmark (filename . ,file))
                 t
                 #'compare-fn)))

;; Make sure the contents of `ensured-bookmark-alist' is available in
;; `bookmark-alist'

(add-hook 'after-init-hook
          (defun ensure-bookmarks ()
            (bookmark-maybe-load-default-file)
            (cl-loop for elem in ensured-bookmark-alist do
                     (have-bookmark (car elem) (cadr elem)))))

(add-to-list 'ensured-bookmark-alist `("emacs-config" ,(file-truename my-config-dir)))
