;;; Ispell  -*- lexical-binding: t; -*-
;;
;; Use `M-x ispell-change-dictionary' to change language
;;
;; Hunspell is used as it seems its the only spell checking program to
;; support multiple languages.
(setq ispell-personal-dictionary (substitute-in-file-name "$SYNCDIR/stavning/stavning"))
(with-eval-after-load 'ispell
  (when (executable-find "hunspell")
    (setq ispell-program-name "hunspell") ; Use Hunspell if available.
    (condition-case err
        (progn (ispell-set-spellchecker-params)       ; ispell-set-spellchecker-params has to be called before ispell-hunspell-add-multi-dic will work
               (ispell-hunspell-add-multi-dic "en_US,sv_SE")
               (setq ispell-dictionary "en_US,sv_SE")) ; Set both Swedish and English dictionaries by default.
      (error (message "Svensk stavningskontroll inte tillgängligt: %s"
                      (error-message-string err))))))

