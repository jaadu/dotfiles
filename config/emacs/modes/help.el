;;;; Packages for providing help

;;; which-key
;;
;; Shows commands
(add-to-list 'package-selected-packages 'which-key)
(when (locate-library "which-key")
  (run-with-idle-timer 2 nil #'which-key-mode 1))
(with-eval-after-load 'which-key
  (if (fboundp 'diminish)
      (diminish 'which-key-mode)))



;;; Helpful
;;
;; https://github.com/Wilfred/helpful
;;
;; Adds more help to describe-* functions
(add-to-list 'package-selected-packages 'helpful)
(with-eval-after-load 'evil
  (add-hook 'emacs-lisp-mode-hook
            (lambda ()
              (setq-local evil-lookup-func #'helpful-at-point))))
(define-key help-map "f" #'helpful-callable)
(define-key help-map "v" #'helpful-variable)
(define-key help-map "k" #'helpful-key)
(define-key help-map "x" #'helpful-command)
(define-key emacs-lisp-mode-map (kbd "C-c C-d") 'helpful-at-point)



(global-eldoc-mode 1)
(setq eldoc-documentation-strategy 'eldoc-documentation-compose-eagerly)
(when (fboundp 'diminish)
  (diminish 'eldoc-mode))



(define-key help-map "A" #'apropos)
(define-key help-map (kbd "C-h") #'nil)
