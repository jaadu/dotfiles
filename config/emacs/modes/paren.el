;; -*- lexical-binding: t; -*-

;;; Electricity
(electric-pair-mode t)                  ; Insert pairs of (), "", [], etc.



;;; Highlight matching parens

(add-to-list 'package-selected-packages 'highlight-parentheses)
(setq show-paren-delay 0
      show-paren-when-point-in-periphery t
      show-paren-when-point-inside-paren t)
(show-paren-mode t)

(setq highlight-parentheses-delay 0
      highlight-parentheses-highlight-adjacent t
      highlight-parentheses-background-colors '("#ccf" "#fcf" "#fcc" "#cfc" "#cff" "#ffc")
      highlight-parentheses-colors nil)
(when (fboundp 'highlight-parentheses-mode)
  (global-highlight-parentheses-mode 1))



;;; Cleverparens
;;
;; Evil specific config for structured editing
;; See (info "(emacs) Parentheses")

;; For alternatives see:
;; - https://dawranliou.com/blog/structural-editing-in-vanilla-emacs/
;; - cleverparens have plenty of bugs, sometimes hangs, sometimes
;;   deletes a paren at the end of the file, and has plenty of
;;   unnecessary keybindings

(add-to-list 'package-selected-packages 'lispyville)
(when (fboundp 'lispyville-mode)
  (setq lispyville-key-theme
        '(operators    ; y, d, c, x, J
          c-w          ; <i>C-w
          text-objects ; <o>ax, <o>ac, <o>af, ...etc
          commentary   ; gc, gy, s-/
          ))
  (with-eval-after-load 'lispyville
    ;; Unbind x, X and s
    (define-key lispyville-mode-map [remap evil-delete-char] nil)
    (define-key lispyville-mode-map [remap evil-delete-char-backwards] nil)
    (define-key lispyville-mode-map [remap evil-substitute] nil)

    (define-key lispyville-mode-map (kbd "M-h") #'lispyville-backward-sexp)
    (define-key lispyville-mode-map (kbd "M-j") #'lispy-down)
    (define-key lispyville-mode-map (kbd "M-k") #'lispyville-backward-up-list)
    (define-key lispyville-mode-map (kbd "M-l") #'lispyville-forward-sexp)
    (define-key lispyville-mode-map (kbd "M-K") #'lispyville-drag-backward)
    (define-key lispyville-mode-map (kbd "M-J") #'lispyville-drag-forward)
    (define-key lispyville-mode-map (kbd "M-H") #'lispyville-barf)
    (define-key lispyville-mode-map (kbd "M-L") #'lispyville-slurp))
  (add-hook 'prog-mode-hook 'lispyville-mode))



;;; Make some lispyville commands available everywhere

;; Text objects
(when (fboundp 'lispyville-mode)
  (with-eval-after-load 'evil
    (progn
      (define-key evil-inner-text-objects-map "a" #'lispyville-inner-atom)
      (define-key evil-inner-text-objects-map "l" #'lispyville-inner-list)
      (define-key evil-inner-text-objects-map "x" #'lispyville-inner-sexp)
      (define-key evil-inner-text-objects-map "f" #'lispyville-inner-function)
      (define-key evil-inner-text-objects-map "c" #'lispyville-inner-comment)
      (define-key evil-inner-text-objects-map "S" #'lispyville-inner-string)
      (define-key evil-inner-text-objects-map "q" #'lispyville-inner-string))
    (progn
      (define-key evil-outer-text-objects-map "a" #'lispyville-a-atom)
      (define-key evil-outer-text-objects-map "l" #'lispyville-a-list)
      (define-key evil-outer-text-objects-map "x" #'lispyville-a-sexp)
      (define-key evil-outer-text-objects-map "f" #'lispyville-a-function)
      (define-key evil-outer-text-objects-map "c" #'lispyville-a-comment)
      (define-key evil-outer-text-objects-map "S" #'lispyville-a-string)
      (define-key evil-outer-text-objects-map "q" #'lispyville-a-string))
    ;; Commentary
    (progn
      (define-key evil-normal-state-map "gc" #'lispyville-comment-or-uncomment)
      (define-key evil-normal-state-map "gy" #'lispyville-comment-and-clone-dwim)
      (define-key evil-visual-state-map "gc" #'lispyville-comment-or-uncomment)
      (define-key evil-visual-state-map "gy" #'lispyville-comment-and-clone-dwim))))



;;; evil-surround: port of vim-surround, vim-sandwich

;; Use with `ys`, cs or ds, or in visual mode: `S`
(add-to-list 'package-selected-packages 'evil-surround)
(when (fboundp 'evil-surround-mode)
  (autoload #'evil-surround-edit "evil-surround" nil t)
  (autoload #'evil-Surround-edit "evil-surround" nil t)
  (autoload #'evil-surround-region "evil-surround" nil t)
  (autoload #'evil-Surround-region "evil-surround" nil t)

  ;; TODO `keyboard-quit' should probably be switched for something else
  (defun evil-sandwich-change ()
    (interactive)
    (and (eq 'evil-surround-region evil-this-operator)
         (call-interactively 'evil-surround-change))
    (keyboard-quit))

  (defun evil-sandwich-delete ()
    (interactive)
    (and (eq 'evil-surround-region evil-this-operator)
         (call-interactively 'evil-surround-delete))
    (keyboard-quit))

  ;; Keybinds like vim-sandwich: sc, and sd
  (define-key evil-operator-state-map "r" 'evil-sandwich-change)
  (define-key evil-operator-state-map "c" 'evil-sandwich-change)
  (define-key evil-operator-state-map "d" 'evil-sandwich-delete)
  (define-key evil-normal-state-map "s" 'evil-surround-region)
  (define-key evil-normal-state-map "S" 'evil-Surround-region)

  (define-key evil-visual-state-map "s" 'evil-surround-region)
  (define-key evil-visual-state-map "S" 'evil-Surround-region)

  ;; Keybinds like vim-surround: ys, cs and ds
  (define-key evil-operator-state-map "s" 'evil-surround-edit)
  (define-key evil-operator-state-map "S" 'evil-Surround-edit)
  (define-key evil-operator-shortcut-map "c" 'evil-surround-change)
  (define-key evil-operator-shortcut-map "d" 'evil-surround-delete)

  (with-eval-after-load 'evil-surround
    (add-hook 'emacs-lisp-mode-hook
              (defun add-surround-doc-pair ()
                (push '(?` . ("`" . "'")) evil-surround-pairs-alist)))))
