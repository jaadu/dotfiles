;; -*- lexical-binding: t; -*-

;; Use perl-compatible regular expressions when using regex builder

(if (fboundp 'pcre-mode)
    (setq reb-re-syntax 'pcre))
