;;; -*- lexical-binding: t; -*-

(setq scheme-program-name "guile")
(with-eval-after-load 'scheme
  (define-key scheme-mode-map (kbd "C-c C-c") #'scheme-compile-definition))
