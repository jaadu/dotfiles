;; -*- lexical-binding: t; -*-
(with-eval-after-load 'comint
  (define-key comint-mode-map (kbd "C-d") nil)
  (when (locate-library "consult")
    (define-key comint-mode-map (kbd "M-r") #'consult-history)))



;; So that interactive shells don't break emacs poorly featured comint-mode.
(setenv "PAGER" "cat")

;; comint modes history does not use the inferior shells history but
;; instead use emacs environment variable, so set histfile here to
;; make sure it is found
(unless (getenv "HISTFILE")
  (setenv "HISTFILE" "$HOME/.config/bash_history" t)
  (setenv "HISTSIZE" "1000"))



(with-eval-after-load 'shell
  (add-hook 'shell-mode-hook
            (defun set-default-shell-buffer-name ()
              "Set the name of the current buffer if its the default `*shell*'."
              (when (string= (buffer-name) "*shell*")
                (rename-buffer
                 (format "*shell:%s*"
                         (file-name-base (directory-file-name default-directory)))
                 t)))))



(add-hook 'compilation-filter-hook 'ansi-color-compilation-filter)
