;; -*- lexical-binding: t; -*-

;;; Settings for communication packages and internet goodies other
;;; than mail.



;;; Circe
;;
;; A irc client for emacs, not built in.
(prog1 'circe
  (setq circe-server-buffer-name "circe-{host}:{port}"))



;;; Erc
;; erc is the emacs builtin irc client
;;
;; Make C-c RET (or C-c C-RET) send messages instead of RET.
;;
;; Use variable `erc-nick` to set username and see the file
;; "~/.authinfo" to set the password.
(prog1 'erc
  (setq erc-autojoin-timing 'ident)
  (setq erc-fill-function 'erc-fill-static
        erc-fill-static-center 12
        erc-insert-timestamp-function 'erc-insert-timestamp-left)
  (setq erc-rename-buffers t
        erc-interpret-mirc-color t
        erc-hide-list '("JOIN" "PART" "QUIT")
        erc-lurker-hide-list '("JOIN" "PART" "QUIT"))
  (setq erc-prompt-for-nickserv-password nil
        erc-prompt-for-password nil)    ; use `auth-sources' instead
  (setq erc-track-exclude-types '("JOIN" "MODE" "NICK" "PART" "QUIT"
                                  "324" "329" "332" "333" "353" "477"))
  (setq erc-fill-column 72)
  (with-eval-after-load 'erc
    (add-hook 'erc-mode-hook #'toggle-truncate-lines)
    (add-hook 'erc-mode-hook #'toggle-word-wrap)
    (add-to-list 'erc-modules 'notifications)
    (add-to-list 'erc-modules 'spelling)
    (erc-services-mode 1)
    (erc-update-modules)
    (set-face-attribute 'erc-default-face nil :family "dejavu sans")
    (set-face-attribute 'erc-input-face nil :family "dejavu sans")
    (define-key erc-mode-map (kbd "C-c RET") 'erc-send-current-line)
    (define-key erc-mode-map (kbd "C-c <C-return>") 'erc-send-current-line)
    (define-key erc-mode-map (kbd "C-c C-c") 'erc-send-current-line)))



;;; Elfeed
;;
;; A package for browsing rss feeds
(prog1 'elfeed
  (setq elfeed-video-program "vlc")
  (with-eval-after-load 'elfeed
    (define-key elfeed-search-mode-map "J" #'eila/elfeed-searches-map)
    (define-key elfeed-show-mode-map "J" #'eila/elfeed-searches-map)
    (define-key elfeed-search-mode-map "v" #'my-elfeed-vlc)
    (define-key elfeed-search-mode-map (kbd "C-c C-u") #'elfeed-update)
    (load (substitute-in-file-name "$SYNCDIR/elfeeds-feeds.el") t)))
(defun my-bitchute-videourl (userid videoid)
  "Returns a valid bitchute video link"
  (format "https://seed%02d.bitchute.com/%s/%s.mp4" (random 30) userid videoid))
(defun my-elfeed-vlc ()
  "Open link in vlc"
  (interactive)
  (let ((entry (elfeed-search-selected :single)))
    (elfeed-untag entry 'unread)
    (start-process "elfeed-video" nil elfeed-video-program (elfeed-entry-link entry))
    (elfeed-search-update-entry entry)))



;;; Ement
;;
;; A matrix client for emacs. Relies on a separate program
;; (pantalaimon) for e2ee.

(defalias 'ement 'ement-connect)

(setq ement-save-sessions t)

(add-to-list 'package-selected-packages 'ement)
