;;; -*- lexical-binding: t; -*-

;; Setup for moving around in files, buffers and windows

;;; Alert
;;
;; Show various alerts. Use with `(alert)'.
(setq alert-default-style 'notifications)



;;; Ibuffer
;;
;; Interactive mode for managing buffers
(add-to-list 'package-selected-packages 'ibuffer-project)
(when (fboundp 'ibuffer-project-generate-filter-groups)
  (add-hook 'ibuffer-hook
   (defun ibuffer-filter-groups-by-projects ()
     (setq ibuffer-filter-groups (ibuffer-project-generate-filter-groups))
     (unless (eq ibuffer-sorting-mode 'project-file-relative)
       (ibuffer-do-sort-by-project-file-relative)))))
(setq ibuffer-filter-groups
      '(("default"
         ("emacs" (or (name . "^\\*scratch\\*$")
                      (name . "^\\*Messages\\*$")))
         ("special" (starred-name)))))


