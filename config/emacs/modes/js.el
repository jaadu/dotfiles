;; -*- lexical-binding: t; -*-

(add-to-list 'auto-mode-alist '("\\.tsx?\\'" . javascript-mode))
(add-to-list 'auto-mode-alist '("\\.tsx?\\'" . typescript-ts-mode))

(add-to-list 'auto-mode-alist '("\\.prisma\\'" . prisma-ts-mode))
