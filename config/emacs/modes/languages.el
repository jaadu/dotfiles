;;; -*- lexical-binding: t; -*-

;; Settings for programming langauges
(prog1 'prog-mode
  (add-hook 'prog-mode-hook #'display-line-numbers-mode))

(prog1 'python
  (setq python-shell-interpreter "python3"
        python-check-command "pycodestyle"))

(prog1 'ruby
  (add-to-list 'auto-mode-alist '("\\.pp?\\'" . ruby-mode)))



;;; Subword mode
;;
;; - Subword: Move by Camel-/ Pascal-cased words
;; - Superword: Skip over _ and -
;;
;; They are both mutually exclusive
(global-subword-mode)

