;;;; Info  -*- lexical-binding: t; -*-

(with-eval-after-load 'evil
  (evil-define-key '(motion normal motion-plus) Info-mode-map
    (kbd "C-o") 'Info-history-back
    (kbd ",") 'Info-index-next
    (kbd "n") 'Info-next))
(with-eval-after-load 'info
  (if (fboundp 'consult-info)
      (define-key Info-mode-map "s" 'consult-info))
  (add-hook 'Info-mode-hook #'variable-pitch-mode)
  (set-face-attribute 'Info-quoted nil
                      :background "white smoke"
                      :box '(:line-width 1 :color "dark gray")))



;; Broken, I should probably use an external program like screenkey
(defcustom lossage-mode-buffer
 "*lossage*"
 "The name of `show-lossage-mode' buffer")

(define-minor-mode show-lossage-mode
  "Show a buffer in a side window showing keystrokes"
  :global t
  (if show-lossage-mode
      (progn
        (view-lossage)
        (display-buffer-in-side-window
         (with-current-buffer "*Help*"
           (rename-buffer lossage-mode-buffer)
           (local-set-key (kbd "q") #'show-lossage-mode-exit))
         '((side . right)
           (width . 80)))
        (add-hook 'post-command-hook #'show-lossage-update-buffer))
    (progn
      (kill-buffer lossage-mode-buffer)
      (remove-hook 'post-command-hook #'show-lossage-update-buffer))))

(defun show-lossage-mode-exit ()
  (interactive)
  (show-lossage-mode -1)
  (kill-buffer lossage-mode-buffer))

(defun show-lossage-update-buffer ()
    "Update the `lossage-mode-buffer' buffer.

For this to work, visit the lossage buffer, and call M-x
rename-buffer Lossage RET"
    (save-excursion
      (let ((b (get-buffer lossage-mode-buffer)))
        (when (buffer-live-p b)
          (with-current-buffer b
            (revert-buffer nil 'noconfirm))))))
