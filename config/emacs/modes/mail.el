;;; -*- lexical-binding: t; -*-

;; mu4e is a frontent for mu, a program for indexing emails. mu4e is
;; dependant on mu, which is installed separately.

(when-let (mu-executable (executable-find "mu"))
  (unless (string-match-p "guix-profile" mu-executable)
    (add-to-list 'load-path (substitute-in-file-name "$HOME/.local/share/emacs/site-lisp/mu4e"))
    (add-to-list 'load-path (substitute-in-file-name "/usr/share/emacs/site-lisp/mu4e")))
  (require 'mu4e-autoloads)
  (setq-default mail-user-agent 'mu4e-user-agent
                message-kill-buffer-on-exit t
                mu4e-attachment-dir "~/Downloads"
                mu4e-change-filenames-when-moving t ; mbsync expects this
                mu4e-compose-context-policy 'ask
                mu4e-confirm-quit nil
                mu4e-context-policy 'pick-first
                mu4e-headers-show-threads t
                mu4e-headers-visible-columns 80
                mu4e-modeline-show-global nil
                mu4e-search-include-related nil ; Do not show related posts (from the wrong mailboxes).
                mu4e-split-view nil
                mu4e-use-fancy-chars nil
                read-mail-command 'mu4e
                shr-inhibit-images t)
  (defvaralias 'mu4e-completing-read-function 'completing-read-function)
  (defvaralias 'mu4e-compose-signature 'message-signature)
  (defvaralias 'mu4e-compose-cite-function 'message-cite-function))
(with-eval-after-load "mu4e"
  (define-key mu4e-compose-mode-map (kbd "C-c M-q") #'mu4e-compose-format-flowed-mode)
  (define-key mu4e-headers-mode-map (kbd "J") #'mu4e-search-maildir)
  (define-key mu4e-main-mode-map "J" 'mu4e-search-maildir)
  (define-key mu4e-main-mode-map "U" #'mu4e-update-all-mail-and-index)
  (define-key mu4e-view-mode-map (kbd "C-c C-o") #'mu4e~view-browse-url-from-binding)
  (define-key mu4e-view-mode-map (kbd "M-q") #'gnus-article-fill-long-lines)

  (add-to-list 'mu4e-bookmarks (list
                                :name "All inboxes and subdirectories"
                                :query "maildir:/Inbox/"
                                :key ?I))
  (add-to-list 'mu4e-bookmarks (list
                                :name "All inboxes"
                                :query "maildir:/Inbox$/"
                                :key ?i))
  (add-to-list 'mu4e-bookmarks (list
                                :name "Drafts"
                                :query "maildir:/Drafts$/ or maildir:/Utkast$/"
                                :key ?d)
               :append)

  (add-hook 'mu4e-compose-mode-hook #'mu4e-compose-format-flowed-mode)

  (with-eval-after-load 'evil
    (evil-define-key 'motion-plus mu4e-view-mode-map
      "J" 'mu4e-search-maildir
      ;; try to emulate some of the eww key-bindings
      (kbd "RET") #'mu4e--view-browse-url-from-binding
      (kbd "<tab>") #'shr-next-link
      (kbd "<backtab>") #'shr-previous-link)
    (add-hook 'evil-list-state-entry-hook
              (defun mu4e-unset-r ()
                (when (equal major-mode 'mu4e-headers-mode)
                  (define-key evil-list-state-local-map "r" nil)))
              95)))



(defun mu4e-update-all-mail-and-index (run-in-background)
  "Like `mu4e-update-mail-and-index' but with all mailboxes. "
  (interactive "P")
  (let ((mu4e-get-mail-command "mbsync -a"))
    (mu4e-update-mail-and-index run-in-background)))

(defun mu4e-message-function (regexp)
  "Returns a function for use in `mu4e-context''s :match-func.

Needs to have `lexical-binding' set to true. "
  (lambda (msg)
    (when msg
      (string-match-p regexp (mu4e-message-field msg :maildir)))))



(defun mu4e-switch-header-field ()
  ;; TODO
  "Switch header field between predefined variables."
  (interactive)
  (setq mu4e-headers-fields '((:human-date . 12)
                              (:flags . 6)
                              (:maildir . 20)
                              (:from . 22)
                              (:subject))))

(defcustom mu4e-add-before-field
  :flags
  "Before which field a new field should be added by `mu4e-headers-add-field'.")

;; (assq
;;  mu4e-add-before-field
;;  mu4e-headers-fields)

;; TODO: Make sure add-before-field is being used.
;; TODO: Create a loop to show appropiate values for completing read
(defun mu4e-headers-add-field ()
  "Add given field to header view."
  (interactive)
  (let* ((field (alist-get
                 (intern
                  (completing-read "Header: " mu4e-header-info))
                 mu4e-header-info))
         (size (read-minibuffer "Width: ")))
    (add-to-list 'mu4e-headers-fields `(,field . ,size))
    (mu4e-headers-rerun-search)))

;; TODO: Use collection
(defun mu4e-headers-remove-field ()
  "Remove given field from header view."
  (interactive)
  (setq mu4e-headers-fields
        (assq-delete-all
         (intern (completing-read "Remove field: " mu4e-headers-fields nil :require-match))
         mu4e-headers-fields))
  (mu4e-headers-rerun-search))



(define-minor-mode mu4e-compose-format-flowed-mode
  "Whether outgoing messages use format flowed or not.

Format flowed means that the recievers email client is
responsible for reflowing incoming messages.

Switches `mu4e-compose-format-flowed'."
  :lighter " fflowed"
  (if mu4e-compose-format-flowed-mode
      (progn
        (setq-local mu4e-compose-format-flowed t)
        (auto-fill-mode -1)
        (visual-line-mode 1))
    (progn
      (setq-local mu4e-compose-format-flowed nil)
      (auto-fill-mode 1)
      (visual-line-mode -1))))



;;; message - Writing Emails
(prog1 'message
  (setq message-confirm-send t
        message-from-style 'angles
        message-kill-buffer-on-exit t
        message-sendmail-f-is-evil t)
  (setq message-citation-line-function 'message-insert-formatted-citation-line
        message-citation-line-format "%A den %d %B %Y skrev %N:\n"))



;;; Builtin package for sending smtp-emails
(setq send-mail-function 'smtpmail-send-it ; Emacs builtin smtp-library
      smtpmail-debug-info t
      smtpmail-smtp-service 587
      smtpmail-stream-type 'starttls
      mail-interactive t)



;; Make sure to not use old date headers
(add-hook 'message-send-hook
          (defun message-remove-date-header ()
            (message-remove-header "^Date:" t)))

;; Confirm before sending mail with empty subject
(add-hook 'message-send-hook
          (defun message-check-subject ()
            (or (message-field-value "Subject")
                (yes-or-no-p "Really send without Subject? ")
                (progn
                  (mail-subject)
                  (keyboard-quit)))))

