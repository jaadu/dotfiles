;;; web.el --- A major mode for web templates        -*- lexical-binding: t; -*-
;;
;; Also see js.el, prog.el and skewer.el

(add-to-list 'package-selected-packages 'flymake-collection)
(when (fboundp 'flymake-collection-html-tidy)
  (add-hook 'html-mode-hook
            (defun html-mode-setup-flymake ()
              (add-hook 'flymake-diagnostic-functions 'flymake-collection-html-tidy nil t)))
  (add-hook 'html-mode-hook #'flymake-mode))

(setq js-indent-level 2)



;; TODO: Add Css linting

(with-eval-after-load 'css-mode
  (define-key css-mode-map (kbd "C-c C-d") #'css-lookup-symbol))



;;; emmet.el
;;
;; insert templates conveniently

(when (locate-library "emmet-mode")
  (add-hook 'web-mode-hook 'emmet-mode)
  (add-hook 'sgml-mode-hook 'emmet-mode)
  (add-hook 'typescript-ts-base-mode-hook 'emmet-mode)
  (with-eval-after-load 'emmet
    (with-eval-after-load 'evil
      (add-hook 'evil-insert-state-exit-hook #'emmet-preview-abort))))



;;; Easily insert html-tags
(defun enable-sgml-quick-keys ()
  (require 'sgml-mode)
  (setq-local sgml-quick-keys t))
(add-hook 'tsx-ts-mode-hook 'use-sgml-quick-keys)
(add-hook 'js-jsx-mode-hook 'use-sgml-quick-keys)

(add-hook 'tsx-ts-mode-hook 'sgml-electric-tag-pair-mode)
(add-hook 'js-jsx-mode-hook 'sgml-electric-tag-pair-mode)
