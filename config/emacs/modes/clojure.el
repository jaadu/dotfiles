;;; Configurations for Clojure

;; Install Clojure anywhere via the script found at:
;; https://clojure.org/guides/getting_started
;;
;; Then add installation bin dir to `exec-path'.

(prog1 'clojure-mode
  ;:mode "\\.clj\\'"
  (setq clojure-align-forms-automatically t)
  (add-hook 'clojure-mode-hook
            (lambda ()
              (setq-local outline-regexp ";;; \\*+ "))))

(prog1 'cider
  (setq cider-debug-display-locals t
        cider-debug-use-overlays 'both)
  (setq cider-repl-display-help-banner nil)
  (setq cider-clojure-cli-global-options "-A:dev:test")
  (setq cider-scratch-initial-message
   ";; -*- mode: cider-clojure-interaction -*- \n")
  (with-eval-after-load 'evil
    (with-eval-after-load 'cider-inspect
      (evil-make-intercept-map 'cider-inspector-mode-map 'motion))
    (add-hook 'cider-popup-buffer-mode-hook #'evil-motion-state) ; `cider-popup-buffer-mode' is a minor mode
    (add-hook 'cider-mode-hook
              (lambda ()
                (setq-local evil-lookup-func #'cider-doc)))
    (add-hook 'cider--debug-mode-hook 'evil-normalize-keymaps)
    (with-eval-after-load 'cider-debug
      (evil-make-intercept-map cider--debug-mode-map 'normal)))
  (with-eval-after-load 'cider
    (with-eval-after-load 'evil
      (evil-define-motion evil-cider-find-var ()
        :jump t
        :type exclusive
        (let ((cider-prompt-for-symbol nil))
          (cider-find-var)))
      (evil-define-key 'normal cider-mode-map "gd" 'evil-cider-find-var))))

