;; -*- lexical-binding: t; -*-
(setq version-control t
      vc-follow-symlinks t)



;; Git client
(add-to-list 'package-selected-packages 'magit)
(define-key shortcut-map "g" #'magit-status)
(define-key shortcut-map "G" #'magit-file-dispatch)
(with-eval-after-load 'evil
  (evil-define-key '(normal motion emacs) 'magit-mode-map
    (kbd "C-d") 'evil-scroll-down
    (kbd "C-u") 'evil-scroll-up))



;; Show vc status on fringes in both buffers and dired
(add-to-list 'package-selected-packages 'diff-hl)
(setq diff-hl-fringe-bmp-function 'diff-hl-fringe-bmp-from-type)
(when (locate-library "diff-hl")
  (global-diff-hl-mode)
  (diff-hl-flydiff-mode)
  (add-hook 'dired-mode-hook 'diff-hl-dired-mode))



(defun vc-git-grep-simple (regexp)
  "Run git grep, searching for REGEXP in directory `vc-root-dir'."
  (interactive (progn (grep-compute-defaults)
                      (list (grep-read-regexp))))
  (vc-git-grep regexp "*" (vc-root-dir)))

(define-key shortcut-map "s" #'vc-git-grep-simple)
(define-key shortcut-map "v" #'vc-dir-root)



(defun vc-git-clone-repo (repository-url local-dir)
  "Run \"git clone REPOSITORY-URL\" to LOCAL-DIR.

Executes `vc-dir' in the newly cloned directory.

Via: https://site.sebasmonia.com/posts/2024-08-15-emacs-vc-mode-tutorial.html"
  (interactive
   (let* ((url (read-string "Repository URL: "))
          (dir (file-name-base url)))
     (list url (read-string "Target directory: " dir))))
  (vc-clone repository-url 'Git local-dir)
  (vc-dir local-dir))
