;; -*- lexical-binding: t; -*-

;; I'm probably better off using the built-in
;; `global-text-scale-adjust'.

(defvar double-font-size--default nil)

(define-minor-mode double-font-size-mode
  "Doubles the font size for all frames."
  :global t
  :lighter " x2"
  (unless double-font-size--default
    (setq double-font-size--default
          (face-attribute 'default :height)))
  (if double-font-size-mode
      (set-face-attribute 'default nil :height (* double-font-size--default 2))
    (set-face-attribute 'default nil :height double-font-size--default)))

