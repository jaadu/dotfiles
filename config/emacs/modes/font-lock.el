;; -*- lexical-binding: t; -*-



(defface hc-tab '((t (:background "LemonChiffon")))
  "Face for highlighting tab characters (`C-i') in Font-Lock mode."
  :group 'Highlight-Characters :group 'faces)

(defun highlight-tabs ()
  (font-lock-add-keywords nil `(("[\t]+" 0 'hc-tab t))))

(add-hook 'text-mode-hook #'highlight-tabs)
(add-hook 'prog-mode-hook #'highlight-tabs)

;; (let ((table (make-display-table)))
;;   (aset table ?\^i (vconcat " ->     "))
;;   (setq-default buffer-display-table table))



;; Display the formfeed ^L char as line.
;; Inspired by `http://ergoemacs.org/emacs/emacs_form_feed_section_paging.html'
(defface hc-ff '((t (:extend t :strike-through t :inherit font-lock-comment-face)))
  "Face for highlighting form feed charactes (`C-l') in Font-Lock mode."
  :group 'Highlight-Characters :group 'faces)

(defun highlight-ff ()
  (font-lock-add-keywords nil `(("+\n" 0 'hc-ff t))))

(add-hook 'text-mode-hook #'highlight-ff)
(add-hook 'prog-mode-hook #'highlight-ff)



(defface hc-non-ascii '((t (:background "LightBlue1")))
  "Face for highlighting non-ascii characters in Font-Lock mode."
  :group 'Highlight-Characters :group 'faces)

(define-minor-mode highlight-non-ascii-mode
  "Highlight non-ascii characters in current buffer."
  :lighter " hl-åäö"
  (if highlight-non-ascii-mode
      (font-lock-add-keywords nil `(("[[:nonascii:]]+" 0 'hc-non-ascii t)))
    (font-lock-remove-keywords nil `(("[[:nonascii:]]+" 0 'hc-non-ascii t))))
  (font-lock-update))
