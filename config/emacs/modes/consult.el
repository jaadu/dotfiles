;; -*- lexical-binding: t; -*-

;; Enhanced commands with minibuffer completion
(add-to-list 'package-selected-packages 'consult)
(when (fboundp 'consult-line)
  (define-key global-map (kbd "C-M-s") #'consult-line)
  (define-key shortcut-map (kbd "S") #'consult-git-grep)
  (define-key shortcut-map (kbd "b") #'consult-buffer)
  (define-key shortcut-map (kbd "m") #'consult-imenu)
  (define-key minibuffer-mode-map (kbd "M-r") #'consult-history)
  ;; Use minibuffer for completion-at-point
  (setq completion-in-region-function #'consult-completion-in-region))

