;; Dired  -*- lexical-binding: t; -*-
;;
;; The built-in file browser
;;
;; Bug: If `dired-listing-switches' is set to
;; --group-directories-first dired will break on remote BSD systems
(setq dired-dwim-target t
      dired-vc-rename-file t
      wdired-allow-to-change-permissions t)
(setq-default dired-listing-switches "-alh --group-directories-first")
(add-hook 'dired-mode-hook #'hl-line-mode)
(autoload 'gnus-dired-attach "gnus-dired")
(with-eval-after-load 'dired
  (define-key dired-mode-map "b" #'bookmark-jump)
  (define-key dired-mode-map "f" #'find-file)
  (define-key dired-mode-map (kbd "C-c C-a") #'gnus-dired-attach)
  (add-hook 'dired-mode-hook 'auto-revert-mode))
(with-eval-after-load 'evil
  (evil-define-key 'list dired-mode-map
    "h" 'dired-up-directory
    "l" 'dired-find-alternate-file
    "K" 'dired-kill-line))
