;;;; Text  -*- lexical-binding: t; -*-

(add-hook 'text-mode-hook #'auto-fill-mode)
(add-hook 'text-mode-hook #'flyspell-mode)



(add-to-list 'package-selected-packages 'mixed-pitch)
(if (fboundp 'mixed-pitch-mode)
    (add-hook 'text-mode-hook #'mixed-pitch-mode)
  (add-hook 'text-mode-hook #'variable-pitch-mode))



(defun electric-pair-add-dollar-signs ()
  (setq-local electric-pair-pairs
              (append electric-pair-pairs '((?$ . ?$)))))



;; https://jblevins.org/projects/markdown-mode/
(prog1 'markdown-mode
  (add-to-list 'package-selected-packages 'markdown-mode)
  (when (fboundp 'markdown-mode)
    (add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode)))
  (setq markdown-command "pandoc"))

(add-hook 'markdown-mode-hook
          (defun electric-pair-add-backquoute ()
            (setq-local electric-pair-pairs
                        (append electric-pair-pairs '((?` . ?`))))))



(defun pandoc-compile-markdown-to-pdf ()
  "Exports the current file to a pdf.

Uses pandoc and latex. "
  (interactive)
  (shell-command
   (concat "pandoc "
           (buffer-name)
           " -f markdown -t latex -o "
           (file-name-base (buffer-name))
           ".pdf")))
