;; -*- lexical-binding: t; -*-

;; Completion in a popup window
;;
;; 2024-10-11 Currently doesn't correctly shadows evil-modes bindings
(prog1 'corfu
  (when (locate-library "corfu")
    (setq corfu-cycle t
          corfu-quit-at-boundary nil
          corfu-quit-no-match nil
          corfu-on-exact-match nil
          corfu-preview-current nil
          corfu-echo-delay 0.01)
    (with-eval-after-load 'corfu
      (define-key corfu-map (kbd "C-d") 'corfu-scroll-up)
      (define-key corfu-map (kbd "C-u") 'corfu-scroll-down)
      (define-key corfu-map (kbd "C-h") 'corfu-info-documentation)
      (define-key corfu-map (kbd "C-n") 'corfu-next)
      (define-key corfu-map (kbd "C-p") 'corfu-previous)
      (define-key corfu-map " " 'corfu-insert-separator)
      (define-key corfu-map "-" 'corfu-insert-separator)
      (define-key corfu-map [escape] 'corfu-quit)
      (with-eval-after-load 'evil
        (add-hook 'evil-normal-state-entry-hook 'corfu-quit)
        ;; This is a hack. See:
        ;; https://github.com/minad/corfu/issues/12
        (evil-make-overriding-map corfu-map)
        (advice-add 'corfu--setup :after (lambda (&rest _) (evil-normalize-keymaps)))
        (advice-add 'corfu--teardown :after (lambda (&rest _) (evil-normalize-keymaps)))))
    (global-corfu-mode 1)
    (corfu-echo-mode 1)))
