;;; -*- lexical-binding: t; -*-

;; Config for builtin mode ediff
(prog1 'ediff
  (setq ediff-window-setup-function 'ediff-setup-windows-plain
        ediff-split-window-function 'split-window-horizontally))
