;; -*- lexical-binding: t; -*-

(with-eval-after-load 'image-mode
  (define-key image-mode-map "+" #'image-increase-size)
  (define-key image-mode-map "-" #'image-decrease-size))
