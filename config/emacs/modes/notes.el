;; -*- lexical-binding: t; -*-

(setq note-directory "~/notes")
(defvaralias 'denote-directory 'note-directory)
(when (fboundp 'denote)
  (setq denote-known-keywords nil)
  (define-key shortcut-map "n" #'denote))

(defun dired-find-note-directory ()
  "Go to personal notes."
  (interactive)
  (dired note-directory))
(define-key shortcut-map "N" #'dired-find-note-directory)
(add-to-list 'ensured-bookmark-alist `("Notes" ,note-directory))
