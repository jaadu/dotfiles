;; -*- lexical-binding: t; -*-
(define-key 'shortcut-map (kbd "f") #'project-find-file)
(define-key 'shortcut-map (kbd "o") #'project-find-regexp)
(with-eval-after-load 'project
  (define-key project-prefix-map "m" #'magit-status)
  (add-to-list 'project-switch-commands '(magit-status "Magit status"))
  (define-key project-prefix-map "S" #'consult-git-grep)
  (add-to-list 'project-switch-commands '(consult-git-grep "Git grep"))
  (delete '(project-eshell "Eshell") project-switch-commands))
