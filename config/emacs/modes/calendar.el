;;;  -*- lexical-binding: t; -*-

;;; Setup for builtin mode calendar
;; Weeks start on mondays
(setq calendar-week-start-day 1)

;; Show week
(setq calendar-intermonth-text
      '(propertize
        (format "%2d"
                (car
                 (calendar-iso-from-absolute
                  (calendar-absolute-from-gregorian (list month day year)))))
        'font-lock-face 'font-lock-function-name-face))

;; Via https://emacs.stackexchange.com/a/42533
(defun calendar-insert-date ()
  "Capture the date at point, exit the Calendar, insert the date."
  (interactive)
  (let ((date (save-match-data (calendar-cursor-to-date))))
    (calendar-exit)
    (insert (calendar-date-string date))))

(with-eval-after-load 'calendar
  (setq calendar-date-display-form calendar-european-date-display-form)
  (define-key calendar-mode-map (kbd "RET") 'calendar-insert-date))
