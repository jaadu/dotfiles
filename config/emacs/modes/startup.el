;; -*- lexical-binding: t; -*-

;; How startup happens

(setq inhibit-startup-screen t) ;; scratch will have to do as a startup screen



;; Show bookmarks in scratch-buffer
(require 'bookmark)

(defun populate-scratch-buffer ()
  (with-current-buffer (get-scratch-buffer-create)
    (insert (substitute-command-keys initial-scratch-message))
    (insert ";; Bookmarks:\n")
    (bookmark-maybe-load-default-file)
    (dolist (bookmark bookmark-alist)
      (insert-text-button (car bookmark)
                          'action (lambda (_) (bookmark-jump bookmark)))
      (insert "\n"))
    (insert "\n;; Mail:\n")
    (insert-text-button "mu4e" 'action
                        (lambda (&rest _)
                          (call-interactively #'mu4e)))
    (insert "\n")
    (insert-text-button "Compose new Mail" 'action #'compose-mail)
    (insert "\n\n;; elisp:\n")
    (insert-text-button "shortdoc" 'action #'(lambda (&rest _) (call-interactively #'shortdoc)))
    (insert "\n\n")
    (set-buffer-modified-p nil)))

(add-hook 'after-init-hook 'populate-scratch-buffer)
