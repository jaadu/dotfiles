;; -*- lexical-binding: t; -*-

(add-hook 'tex-mode-hook 'flymake-mode)

(with-eval-after-load 'flymake
  (define-key flymake-mode-map (kbd "M-n") #'flymake-goto-next-error)
  (define-key flymake-mode-map (kbd "M-p") #'flymake-goto-prev-error))

