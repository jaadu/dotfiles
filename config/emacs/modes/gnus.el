;;; -*- lexical-binding: t; -*-

;; Newsreader
(prog1 'gnus
  (setq-default gnus-summary-line-format "%U%R%z %(%&user-date;  %-15,15f  %B%S%)\n"
                gnus-user-date-format-alist '((t . "%Y-%m-%d")))
  (with-eval-after-load 'gnus
    (define-key gnus-article-mode-map "q" #'delete-window))
  (add-hook 'gnus-article-mode-hook #'variable-pitch-mode)
  (add-hook 'gnus-summary-mode-hook #'hl-line-mode))

