;; -*- lexical-binding: t; -*-

(when (locate-library "graphviz-dot-mode")
  (setq graphviz-dot-preview-extension "svg"))
