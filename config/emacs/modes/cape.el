;; -*- lexical-binding: t; -*-

;; Additional completion functions
(prog1 'cape
  (add-to-list 'package-selected-packages 'cape)
  (when (locate-library "cape")
    (define-key global-map (kbd "M-+") #'cape-file)
    (with-eval-after-load 'evil
      (define-key evil-insert-state-map (kbd "C-p") #'cape-dabbrev))))
