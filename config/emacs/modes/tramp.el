;; -*- lexical-binding: t; -*-

(setq-default tramp-default-remote-shell "/bin/bash"
              tramp-encoding-shell "/bin/bash")
