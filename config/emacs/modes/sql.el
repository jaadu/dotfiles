;; -*- lexical-binding: t; -*-

(add-to-list 'package-selected-packages 'sqlup-mode)
(when (locate-library "sqlup-mode")
  (add-hook 'sql-mode-hook 'sqlup-mode))

;; https://www.emacswiki.org/emacs/SqlMode#h5o-5
(add-hook
 'sql-login-hook
 (defun sql-fix-postgresql-prompt ()
   "Custom SQL log-in behaviours. See `sql-login-hook'."
   ;; n.b. If you are looking for a response and need to parse the
   ;; response, use `sql-redirect-value' instead of `comint-send-string'.
   (when (eq sql-product 'postgres)
     (let ((proc (get-buffer-process (current-buffer))))
       ;; Output each query before executing it. (n.b. this also avoids
       ;; the psql prompt breaking the alignment of query results.)
       (comint-send-string proc "\\set ECHO queries\n")))))

