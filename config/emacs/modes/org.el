;; -*- lexical-binding: t; -*-

;;; org.el --- Org mode setup



;;; general settings
(setq org-adapt-indentation nil ; Do not indent automatically by outline
      org-catch-invisible-edits 'show-and-error
      org-edit-src-content-indentation 0
      org-special-ctrl-a/e t
      org-startup-folded 'content)
(setq org-list-demote-modify-bullet '(("-" . "+")
                                      ("+" . "*")
                                      ("*" . "-")
                                      ("1." . "-")
                                      ("1)" . "-")))
(add-hook 'org-mode-hook #'electric-pair-add-dollar-signs)
;; Why do org-autoloads not load?
(autoload #'orgtbl-mode "org-table" nil t)



;;; faces
(with-eval-after-load 'org-faces
  (set-face-attribute 'org-block nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-code nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-formula nil :inherit 'org-table))



;;; org-agenda
(setq org-scheduled-past-days 0 ; Only show scheduled things on the same day
      org-agenda-skip-deadline-if-done t
      org-agenda-skip-scheduled-if-done t
      org-agenda-todo-ignore-scheduled 'future)



;;; org-babel
(with-eval-after-load 'ob
  (add-to-list 'org-babel-load-languages '(emacs-lisp . t)))



;;; org-export
(setq org-export-default-language "sv"
      org-export-backends '(ascii beamer html icalendar latex md odt)
      ;; TODO: When disabled also prevents toc
      org-export-with-section-numbers t)
(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-packages-alist '("swedish" "babel"))
  ;; Improves kerning and whatnot
  (add-to-list 'org-latex-packages-alist '("" "microtype"))
  (add-to-list 'org-latex-classes
               '("letter" "\\documentclass{letter}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))



;;; org-refile
(setq org-goto-interface 'outline-path-completion
      org-refile-targets '((nil :maxlevel . 9)
                           (org-agenda-files :maxlevel . 9))
      org-refile-use-outline-path 'file
      org-refile-allow-creating-parent-nodes 'confirm
      org-outline-path-complete-in-steps nil)



;;; org-todo
(setq org-todo-keywords '((sequence "TODO(t)" "WAITING(w)" "|" "DONE(d)" "CANCELED(c)")
                          (sequence "|" "PHONE(p)" "MEETING(m)"))
      org-todo-keyword-faces '(("WAITING" . "DarkOrange")
                               ("HOLD" . "Darkorange")
                               ("NEXT" . "blue")
                               ("PHONE" . "blue")
                               ("MEETING" . "blue")))



;;; Bindings
(setq org-agenda-window-setup 'current-window
      org-src-window-setup 'current-window)
(when (fboundp 'org-mode)
  (define-key shortcut-map "a" #'org-agenda)
  (define-key shortcut-map "c" #'org-capture)
  (define-key shortcut-map "l" #'org-store-link)
  (define-key shortcut-map "t" #'org-time-stamp))
(with-eval-after-load 'org
  (with-eval-after-load 'evil
    ;; Inspired by evil-org: https://github.com/Somelauw/evil-org-mode.
    (evil-define-key 'normal org-mode-map
      ;; Base
      (kbd "<tab>") 'org-cycle
      (kbd "<backtab>") 'org-shifttab
      (kbd "RET") 'org-open-at-point
      ;; Additional
      (kbd "M-o") 'org-insert-heading
      ;; Todo
      (kbd "M-t") 'org-todo
      (kbd "M-T") 'org-insert-todo-heading
      ;; Movement
      (kbd "M-h") 'org-metaleft
      (kbd "M-j") 'org-metadown
      (kbd "M-k") 'org-metaup
      (kbd "M-l") 'org-metaright
      (kbd "M-H") 'org-shiftmetaleft
      (kbd "M-J") 'org-shiftmetadown
      (kbd "M-K") 'org-shiftmetaup
      (kbd "M-L") 'org-shiftmetaright
      (kbd "C-S-h") 'org-shiftcontrolleft
      (kbd "C-S-j") 'org-shiftcontroldown
      (kbd "C-S-k") 'org-shiftcontrolup
      (kbd "C-S-l") 'org-shiftcontrolright)))
(with-eval-after-load 'org-colview
  (define-key org-columns-map "g" nil))


