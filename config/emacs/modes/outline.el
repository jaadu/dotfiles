;; -*- lexical-binding: t; -*-
(with-eval-after-load 'outline
  (define-key outline-minor-mode-map (kbd "<tab>") #'outline-cycle)
  (define-key outline-minor-mode-map (kbd "<backtab>") #'outline-cycle-buffer))


;;; hs-minor-mode / hide-show
(add-hook 'sgml-mode-hook 'hs-minor-mode)
(add-hook 'tex-mode-hook 'hs-minor-mode)

(with-eval-after-load 'hideshow
  (define-key hs-minor-mode-map (kbd "<tab>") #'hs-toggle-hiding ))
