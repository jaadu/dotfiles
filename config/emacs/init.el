;;; Basic settings  -*- lexical-binding: t; -*-

;;; Custom
;;
;; Custom is a builtin system used for storing settings set using ~M-x
;; customize~. custom.el is used for testing purposes and any elements
;; are to be considered transient or moved to other config files. This
;; is loaded early as not to overwrite settings in other config files.
(setq custom-file (concat my-config-dir "init/custom.el"))
(load custom-file t)

(load (expand-file-name "init/package"    my-config-dir))
(load (expand-file-name "init/bindings"   my-config-dir))
(load (expand-file-name "init/minibuffer" my-config-dir))
(load (expand-file-name "init/evil"       my-config-dir))
(load (expand-file-name "init/builtins"   my-config-dir))
(load (expand-file-name "init/mode-line"  my-config-dir))

(condition-case e
    (load (expand-file-name "init/secrets.el.gpg" my-config-dir))
  (file-error "Wont load secrets: %s" e))

(setq auth-sources (list (expand-file-name "share/authinfo.gpg" my-config-dir)))

(mapc
 (defun load-despite-error (file)
   (condition-case e
       (load file)
     ((void-function void-variable end-of-file invalid-read-syntax)
      (message "init-error: %s" e))))
 (directory-files (concat my-config-dir "modes")
                  :full "\\.el$" t))
(load (expand-file-name "init/local" my-config-dir) t)

  ;;; Mode settings

(when (file-accessible-directory-p "~/.local/share/emacs/site-lisp")
  (add-to-list 'load-path "~/.local/share/emacs/site-lisp")
  (mapc #'load
        (directory-files "~/.local/share/emacs/site-lisp/" :full "autoloads\\.el$")))

(message "Allt färdigladdat på %s." (emacs-init-time "%f sekunder"))
