;; -*- lexical-binding: t; -*-

;;; Garbage collection
;; Increase the GC threshold for faster startup
;; The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))

(tool-bar-mode 0)                       ; Removes toolbar buttons
(scroll-bar-mode 0)

(setq load-prefer-newer t)



(setq package-selected-packages ())

(defvar my-config-dir user-emacs-directory
  "Personal config directory.

Defaults to `user-emacs-directory' on startup, which in turn is
redirected to ./var.")

(setq-default user-emacs-directory (concat user-emacs-directory "var/")
              package-user-dir (concat user-emacs-directory "elpa"))

