;;; -*- lexical-binding: t; -*-

(define-key global-map [escape] #'keyboard-quit)
(define-key global-map (kbd "<f5>") #'revert-buffer)
(define-key global-map (kbd "C-w") #'backward-kill-word)

;; A shortcut key for useful stuff
(prog1 'shortcut-map
  (define-prefix-command 'shortcut-map)
  (define-key global-map (kbd "C-c") 'shortcut-map)
  (with-eval-after-load 'evil
    (define-key evil-motion-state-map (kbd "ä") 'shortcut-map)
    (define-key evil-emacs-state-map (kbd "ä") 'shortcut-map)))

(define-key shortcut-map "b" #'switch-to-buffer)
(define-key shortcut-map "B" #'ibuffer)
(define-key shortcut-map "f" #'find-file)
(define-key shortcut-map "m" #'imenu)

;;; Completion
(with-eval-after-load 'evil
  (define-key evil-insert-state-map (kbd "C-n") #'completion-at-point)
  (unless (locate-library "cape")
    (define-key evil-insert-state-map (kbd "C-p") #'dabbrev-completion)))
