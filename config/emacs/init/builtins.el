;;; -*- lexical-binding: t; -*-

;; Settings for builtin features

(setq-default major-mode 'text-mode)

;; Enable font locking for many generic buffers like fstab and passwd
(require 'generic-x)



(add-hook 'read-only-mode-hook #'read-only-buffer-setup)
(add-hook 'after-change-major-mode-hook #'read-only-buffer-setup)
(defun read-only-buffer-setup ()
  (setq-local show-trailing-whitespace (not buffer-read-only)))

(setq-default indicate-empty-lines t)



;;; Isearch

(define-key isearch-mode-map (kbd "<S-insert>") #'yank)
(define-key global-map (kbd "C-s") #'isearch-forward-regexp)
(define-key global-map (kbd "C-M-s") #'isearch-forward)



;;; Fonts
(setq font-use-system-font t)

(add-hook 'doc-view-mode-hook 'auto-revert-mode)

;; Merge search and regex search histories
(defalias 'regexp-search-ring 'search-ring)

(setq create-lockfiles nil)
;; auto-save
(setq auto-save-default t
      delete-by-moving-to-trash t
      auto-save-list-file-prefix (concat user-emacs-directory "auto-save-list/.saves-")
      auto-save-file-name-transforms `((".*" ,(concat user-emacs-directory "auto-save/") t)))
;; backup
(setq backup-by-copying t
      delete-old-versions t)
(setq backup-directory-alist
      `(("." . ,(concat
                 user-emacs-directory
                 "backup"))))

(recentf-mode 1)

;; scrolling
(setq scroll-conservatively 10000
      scroll-margin 3)

;;; Display
; Dont wrap lines by default
(setq-default truncate-lines t
              truncate-partial-width-windows nil)

;;; Window
(setq switch-to-buffer-obey-display-actions t)

;;; Indent
(setq-default indent-tabs-mode nil)

;;; Convenience
(setq confirm-kill-emacs 'y-or-n-p
      disabled-command-function nil)
(prefer-coding-system 'utf-8-unix)      ; Use UTF-8 wherever possible.

;;; Fill
(setq sentence-end-double-space nil)    ; Sentences end with a single space.



;;; Visual line mode
(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

(add-to-list 'package-selected-packages 'visual-fill-column)
(when (fboundp 'visual-fill-column-mode)
  (add-hook
   'visual-line-mode-hook
   ;; Ement room user names gets hidden otherwise
   (defun my-visual-fill ()
     (if (and visual-line-mode (not (equal major-mode 'ement-room-mode)))
         (visual-fill-column-mode 1)
       (visual-fill-column-mode -1)))))



;;; Performance

(setq bidi-inhibit-bpa t)
(global-so-long-mode 1)



;; Needs to be hoisted to the top
(defvar ensured-bookmark-alist nil)
