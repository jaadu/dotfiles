;;; mode-line.el ---                                 -*- lexical-binding: t; -*-

;; Allows hiding modes from modeline
(add-to-list 'package-selected-packages 'diminish)

;;; Mode Line
(column-number-mode t)                  ; Shows column number in the modeline

(set-face-attribute 'vc-conflict-state nil :foreground "red")
(set-face-attribute 'vc-edited-state nil :foreground "orange")
