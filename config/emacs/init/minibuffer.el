;;; Minibuffer settings  -*- lexical-binding: t; -*-
;;
;; Interactive interface for completion inside the minibuffer.

(setq enable-recursive-minibuffers t
      echo-keystrokes 0.001)
(define-key minibuffer-mode-map [escape] #'abort-minibuffers)

;; Saves minibuffer history in a file
(savehist-mode 1)



;; Vertical minibuffer interface
(add-to-list 'package-selected-packages 'vertico)
(if (fboundp 'vertico-mode)
    (vertico-mode 1)
  (progn
    (icomplete-mode 1)
    (icomplete-vertical-mode 1)
    (fido-mode 1)
    (fido-vertical-mode 1)))
(setq vertico-cycle t)
(with-eval-after-load 'vertico
  (define-key vertico-map (kbd "C-d") #'vertico-scroll-up)
  (define-key vertico-map (kbd "C-u") #'vertico-scroll-down))

;; (define-key minibuffer-mode-map (kbd "C-d") #'forward-page)
;; (define-key minibuffer-mode-map (kbd "C-u") #'backward-page)
;; (define-key icomplete-fido-mode-map (kbd "C-d") nil)

(setq completions-format 'one-column)
(setq completions-max-height 10)
(setq completion-auto-select nil)



;; Handles ordering and regex-handling for minibuffer commands
(add-to-list 'package-selected-packages 'orderless)
(when (locate-library "orderless")
  (require 'orderless)
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-ignore-case t        ; Workaround to respect case
        orderless-matching-styles '(orderless-literal orderless-regexp)))



;; Provides additional information for many minibuffer commands
(add-to-list 'package-selected-packages 'marginalia)
(when (fboundp 'marginalia-mode)
  (marginalia-mode 1))
