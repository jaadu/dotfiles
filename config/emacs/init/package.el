;;; Basic settings for packages  -*- lexical-binding: t; -*-

;; Declares package archives to check from
(with-eval-after-load 'package
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
  (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
  (setq package-archive-priorities '(("melpa-stable" . 2) ("gnu" . 1)))
  (define-key package-menu-mode-map "s" #'isearch-forward))



(advice-add 'package-install-selected-packages :before #'package-refresh-contents)

;; Suppress compilation errors
(setq warning-suppress-types '((comp)))
