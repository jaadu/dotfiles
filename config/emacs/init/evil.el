;;; -*- lexical-binding: t; -*-

;;; Options and plugins for evil-mode

;; Evil (Emacs VI Layer) is a Vim-emulator.

;; - https://github.com/emacs-evil/evil
;; - https://github.com/noctuid/evil-guide
;;
;; Also see ../modes/evil-addons.el

;;; evil
(setq evil-move-beyond-eol nil ; t for moving by parenthesis properly
      evil-echo-state nil
      evil-symbol-word-search t
      evil-want-Y-yank-to-eol t
      evil-want-C-u-scroll t
      evil-undo-system 'undo-redo)
(add-to-list 'package-selected-packages 'evil)
(when (fboundp 'evil-mode)
  (evil-mode 1)
  (define-key evil-motion-state-map "å" #'evil-window-map)
  (define-key evil-emacs-state-map "å" #'evil-window-map)
  (define-key evil-visual-state-map "å" #'evil-window-map)
  (define-key evil-window-map "å" #'other-window)
  (define-key evil-window-map "f" 'other-frame)
  (define-key evil-window-map "F" 'make-frame-command)
  (define-key evil-motion-state-map (kbd "g f") #'find-file-at-point)
  (define-key evil-normal-state-map (kbd "M-.") nil)
  (define-key evil-visual-state-map "q" #'evil-exit-visual-state)
  (when evil-want-C-u-scroll
    (define-key evil-emacs-state-map (kbd "C-u") 'evil-scroll-up))
  (when evil-want-C-d-scroll
    (define-key evil-emacs-state-map (kbd "C-d") 'evil-scroll-down))
  (define-key shortcut-map "u" 'universal-argument)
  (define-key universal-argument-map (kbd "ä u") 'universal-argument-more)
  (define-key universal-argument-map (kbd "C-c u") 'universal-argument-more)
  (define-key universal-argument-map (kbd "u") 'universal-argument-more))



;;; Transient search commands

;; Add a transient maps for commands that don't bind `n' and `N'
;; normally
(with-eval-after-load 'evil
  (defvar evil-search-transient-map
    (make-sparse-keymap))
  (define-key evil-search-transient-map "n" #'evil-search-next)
  (define-key evil-search-transient-map "N" #'evil-search-previous)

  (defun evil-search-add-transient-map-advice ()
    (set-transient-map evil-search-transient-map t))

  ;; Also works on `evil-search-forward' and all other search functions
  (add-hook 'isearch-mode-end-hook #'evil-search-add-transient-map-advice))



;; https://emacs.stackexchange.com/questions/21110/changing-evil-forward-word-behavior-by-modifying-a-syntax-table
;; Move easily over snake- and kebab-case

(advice-add 'evil-forward-word-begin :after
            (defun skip-dash-forward (&rest _)
              (let ((c (char-after (point))))
                (when (or (eq c ?-)
                          (eq c ?_))
                  (forward-char)))))

(advice-add 'evil-forward-word-end :before
            (defun skip-dash-forward-end (&rest _)
              (let ((c (char-after (+ 1 (point)))))
                (when (or (eq c ?-)
                          (eq c ?_))
                  (forward-char)))))

(advice-add 'evil-backward-word-begin :before
            (defun skip-dash-backward (&rest _)
              (let ((c (char-before (point))))
                (when (or (eq c ?-)
                          (eq c ?_))
                  (backward-char)))))



;;; List state

(with-eval-after-load 'evil
  (evil-define-state list
    "List state.

Used for list-like modes. Changes very little from emacs-state
but adds `j', `k', `g' `G' and `z'. Also rebinds whatever was
bound to `g' to `r' and from `j' to `J'."
    :tag " <L> "
    :suppress-keymap t
    :entry-hook (evil-list-state-dynamic-remap hl-line-mode))

  (progn
    (define-key evil-list-state-map "j" 'evil-next-line)
    (define-key evil-list-state-map "k" 'evil-previous-line)
    (define-key evil-list-state-map "G" 'evil-goto-line)
    (define-key evil-list-state-map "/" 'evil-search-forward)
    (define-key evil-list-state-map "g" (lookup-key evil-motion-state-map "g"))
    (define-key evil-list-state-map "z" (lookup-key evil-motion-state-map "z"))
    (define-key evil-list-state-map (kbd "C-w") 'evil-window-map)

    (when evil-want-C-u-scroll
      (define-key evil-list-state-map (kbd "C-u") 'evil-scroll-up))
    (when evil-want-C-d-scroll
      (define-key evil-list-state-map (kbd "C-d") 'evil-scroll-down)))

  (defvar evil-list-state-dynamic-remaps-alist
    '(("g" "r")
      ("j" "J"))
    "Remap the car of each element in list state to the cadr of said
element.")

  (defun evil-list-state-dynamic-remap ()
    "Remap current mode keys dynamically, if able."
    (dolist (binding evil-list-state-dynamic-remaps-alist)
      (when-let ((command (local-key-binding (car binding))))
        (define-key evil-list-state-local-map (cadr binding) command))))

  (define-key evil-list-state-map "å" evil-window-map)
  (define-key evil-list-state-map "ä" shortcut-map))



;;; Motion plus state

(when (fboundp 'evil-mode)
  (evil-define-state motion-plus
    "Motion plus state.

Used for modes where no editing should be needed, like
`info-mode' and `help-mode'. Removes some bindings from
motion-state and removes 0-9 for prefix keys."
    :tag "<M+> "
    :suppress-keymap t
    :entry-hook (evil-motion-plus-state-dynamic-remap))

  (prog1
      (set-keymap-parent evil-motion-plus-state-map evil-motion-state-map)
    (define-key evil-motion-plus-state-map (kbd "1") nil)
    (define-key evil-motion-plus-state-map (kbd "2") nil)
    (define-key evil-motion-plus-state-map (kbd "3") nil)
    (define-key evil-motion-plus-state-map (kbd "4") nil)
    (define-key evil-motion-plus-state-map (kbd "5") nil)
    (define-key evil-motion-plus-state-map (kbd "6") nil)
    (define-key evil-motion-plus-state-map (kbd "7") nil)
    (define-key evil-motion-plus-state-map (kbd "8") nil)
    (define-key evil-motion-plus-state-map (kbd "9") nil)
    (define-key evil-motion-plus-state-map (kbd "0") nil)
    (define-key evil-motion-plus-state-map (kbd "n") nil)
    (define-key evil-motion-plus-state-map (kbd "RET") nil)
    (define-key evil-motion-plus-state-map (kbd "SPC") nil)
    (define-key evil-motion-plus-state-map (kbd "TAB") nil)
    (define-key evil-motion-plus-state-map (kbd ";") nil)
    (define-key evil-motion-plus-state-map (kbd "?") nil)
    (define-key evil-motion-plus-state-map (kbd "H") nil)
    (define-key evil-motion-plus-state-map (kbd "L") nil)
    (define-key evil-motion-plus-state-map (kbd "M") nil)
    (define-key evil-motion-plus-state-map (kbd "N") nil)
    (define-key evil-motion-plus-state-map (kbd "-") nil))

  (defvar evil-motion-plus-state-dynamic-remaps-alist
    '(("g" "r")
      ("j" "J"))
    "Remap the car of each element in motion-plus state to the cadr
of said element.")

  (defun evil-motion-plus-state-dynamic-remap ()
    "Remap current mode keys dynamically, if able."
    (dolist (binding evil-motion-plus-state-dynamic-remaps-alist)
      (when-let ((command (local-key-binding (car binding))))
        (define-key evil-motion-plus-state-local-map (cadr binding) command)))))



(with-eval-after-load 'evil
  (evil-define-text-object evil-a-dollar
    (count &optional beg end type)
    "Select around double dollar."
    :extend-selection t
    (evil-select-quote ?$ beg end type count t))

  (define-key evil-outer-text-objects-map "$" 'evil-a-dollar)

  (evil-define-text-object evil-inner-dollar
    (count &optional beg end type)
    "Select around double dollar."
    :extend-selection nil
    (evil-select-quote ?$ beg end type count))

  (define-key evil-inner-text-objects-map "$" 'evil-inner-dollar))



;;; Default states

(setq evil-emacs-state-modes '(5x5-mode
                               Custom-mode
                               Electric-buffer-menu-mode
                               archive-mode
                               backtrace-mode
                               bbdb-mode
                               biblio-selection-mode
                               blackbox-mode
                               bookmark-edit-annotation-mode
                               browse-kill-ring-mode
                               bubbles-mode
                               bzr-annotate-mode
                               calc-mode
                               calendar-mode
                               cfw:calendar-mode
                               cider-stacktrace-mode
                               completion-list-mode
                               custom-theme-choose-mode
                               delicious-search-mode
                               desktop-menu-blist-mode
                               desktop-menu-mode
                               diff-mode
                               dun-mode
                               dvc-bookmarks-mode
                               dvc-diff-mode
                               dvc-info-buffer-mode
                               dvc-log-buffer-mode
                               dvc-revlist-mode
                               dvc-revlog-mode
                               dvc-status-mode
                               dvc-tips-mode
                               ediff-meta-mode
                               ediff-mode
                               edraw-mode
                               edraw-property-editor-mode
                               efs-mode
                               elfeed-search-mode
                               emms-browser-mode
                               emms-mark-mode
                               emms-metaplaylist-mode
                               emms-playlist-mode
                               ess-help-mode
                               etags-select-mode
                               fj-mode
                               gc-issues-mode
                               gdb-breakpoints-mode
                               gdb-disassembly-mode
                               gdb-frames-mode
                               gdb-locals-mode
                               gdb-memory-mode
                               gdb-registers-mode
                               gdb-threads-mode
                               gist-list-mode
                               git-rebase-mode
                               gomoku-mode
                               google-maps-static-mode
                               image-mode
                               jde-javadoc-checker-report-mode
                               log-view-mode
                               lyskom-mode
                               magit-section-mode
                               mh-folder-mode
                               monky-mode
                               mpuz-mode
                               mu4e-main-mode
                               notmuch-hello-mode
                               notmuch-search-mode
                               notmuch-show-mode
                               notmuch-tree-mode
                               pdf-outline-buffer-mode
                               pdf-view-mode
                               proced-mode
                               rcirc-mode
                               rebase-mode
                               recentf-dialog-mode
                               reftex-select-bib-mode
                               reftex-select-label-mode
                               reftex-toc-mode
                               simple-mpc-mode
                               sketch-mode
                               sldb-mode
                               slime-inspector-mode
                               slime-thread-control-mode
                               slime-xref-mode
                               sly-db-mode
                               snake-mode
                               solitaire-mode
                               sr-buttons-mode
                               sr-mode
                               sr-tree-mode
                               sr-virtual-mode
                               tar-mode
                               tetris-mode
                               tla-annotate-mode
                               tla-archive-list-mode
                               tla-bconfig-mode
                               tla-bookmarks-mode
                               tla-branch-list-mode
                               tla-browse-mode
                               tla-category-list-mode
                               tla-changelog-mode
                               tla-follow-symlinks-mode
                               tla-inventory-file-mode
                               tla-inventory-mode
                               tla-lint-mode
                               tla-logs-mode
                               tla-revision-list-mode
                               tla-revlog-mode
                               tla-tree-lint-mode
                               tla-version-list-mode
                               twittering-mode
                               undo-tree-visualizer-mode
                               urlview-mode
                               vc-dir-mode
                               vm-mode
                               vm-summary-mode
                               w3m-mode
                               wab-compilation-mode
                               xgit-annotate-mode
                               xgit-changelog-mode
                               xgit-diff-mode
                               xgit-revlog-mode
                               xhg-mode
                               xhg-annotate-mode
                               xhg-log-mode
                               xhg-mq-mode
                               xhg-mq-sub-mode
                               xhg-status-extra-mode))

(setq evil-motion-state-modes '())

(setq evil-normal-state-modes '(git-commit-mode))

(setq evil-list-state-modes '(dired-mode
                              doc-view-mode
                              ement-directory-mode
                              ement-room-list-mode
                              erc-list-mode
                              gnus-group-mode
                              gnus-summary-mode
                              ibuffer-mode
                              mingus-browse-mode
                              mingus-playlist-mode
                              mu4e-headers-mode
                              occur-mode
                              org-agenda-mode
                              tabulated-list-mode
                              tar-mode
                              xref--xref-buffer-mode
                              ztree-mode))

(setq evil-motion-plus-state-modes '(Man-mode
                                     cider-browse-ns-mode
                                     cider-docview-mode
                                     cider-inspector-mode
                                     cider-test-report-mode
                                     color-theme-mode
                                     command-history-mode
                                     compilation-mode
                                     dictionary-mode
                                     elfeed-show-mode
                                     ement-room-mode
                                     eww-mode
                                     geiser-debug-mode
                                     geiser-doc-mode
                                     gnus-article-mode
                                     mu4e-view-mode
                                     skewer-error-mode
                                     sly-inspector-mode
                                     special-mode
                                     speedbar-mode
                                     undo-tree-visualizer-mode
                                     woman-mode))

;; Because message buffer is already loaded when evil is initialized,
;; set it again here
(with-current-buffer messages-buffer-name
  (evil-motion-plus-state))
