# Use super key as universal modifier key
set $mod Mod4

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Shift+Return exec alacritty

# kill focused window
bindsym $mod+q kill

# start dmenu (a program launcher)
bindsym $mod+Return exec dmenu_run -fn sans-serif

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Change focus by tree structure
bindsym $mod+p focus prev
bindsym $mod+n focus next
bindsym $mod+b focus parent
bindsym $mod+f focus child

# focus the parent and child container
bindsym $mod+a focus parent
bindsym $mod+d focus child

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal and vertical orientation
bindsym $mod+g split h
bindsym $mod+s split h
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+F11 fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
# bindsym $mod+s layout stacking
bindsym $mod+w layout stacking
bindsym $mod+t layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# switch to workspace
bindsym $mod+1 workspace browser
bindsym $mod+2 workspace emacs
bindsym $mod+3 workspace terminal
bindsym $mod+4 workspace graphical
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace browser
bindsym $mod+Shift+2 move container to workspace emacs
bindsym $mod+Shift+3 move container to workspace terminal
bindsym $mod+Shift+4 move container to workspace graphical
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# bindsym $mod+Tab          focus next
# bindsym $mod+ISO_Left_Tab focus previous
bindsym $mod+x move workspace to output next

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"
bindsym $mod+Shift+p exec "systemctl suspend"

bindsym $mod+plus resize grow height 10 px or 10 ppt
bindsym $mod+minus resize shrink height 10 px or 10 ppt
bindsym $mod+less resize shrink width 10 px or 10 ppt
bindsym $mod+greater resize grow width 10 px or 10 ppt

# Move the current workspace to the next output
# (effectively toggles when you only have two outputs)
bindsym $mod+c move workspace to output down

# Audio keys
bindsym XF86AudioRaiseVolume exec amixer set Master 5%+
bindsym XF86AudioLowerVolume exec amixer set Master 5%-
bindsym XF86AudioMute        exec amixer set Master toggle
