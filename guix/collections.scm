(define-module (collections))
;; A collection of useful packages
;;
;; Start a repl from shell with:
;;
;;     guix repl --listen=tcp:37146
;;
;; Then connect to it with Emacs:
;;
;;     M-x geiser-connect <ret> <ret> <ret>

(define-public base
  (append
;;; Recommended by (info "(guix) Application Setup")
   (list
    "glibc-swedish-utf8-locale"
    "fontconfig"
    "font-ghostscript"
    "font-dejavu" ; I don't like dejavu very much
    "font-gnu-freefont"
    "nss-certs")
;;; Mandatory things
   (list
    "bash"
    "coreutils"
    "findutils"
    "font-google-noto"
    "font-openmoji"
    "less")
;;; Terminals and shells
   (list
    "alacritty"
    "bash-completion"
    "readline")
;;; Desktop, X
   (list
    "feh"
    "numlockx"
    "xbacklight"
    "xclip"
    "xev"
    "xrandr")
;;; CLI Programs
   (list
    "emacs-motif"
    "git"
    "gnupg"
    ;; "icecat"
    "hunspell"
    "hunspell-dict-en"
    ;; "hunspell-dict-sv" ; Swedish is not available yet :-(
    "info-reader"
    "make"
    "man-db"
    "ncdu"
    "ncmpcpp"
    "pandoc"
    "password-store"
    "qrencode"
    "recutils"
    "screen"
    "sshfs"
    "vim-full")
;;; GUI Programs
   (list
    "mpv"         ; Video player
    "orage"       ; Calendar
    "sgt-puzzles" ; Games
    "xpdf"       ; Pdf reader
    )
;;; Lisp
   (list
    "emacs-sly" ; <2022-07-17>: Seems to be the only slynk package that includes the manual
    "cl-slynk"
    "guile"
    "guile-colorized"
    "guile-readline"
    "sbcl"
    "sbcl-slynk")
 ;;; Other Development tools
   (list
    "rlwrap"
    "shellcheck"
    "tidy-html")
 ;;; Daemons
   (list
    "shepherd"
    "redshift"
    "syncthing")
 ;;; Mail
   (list
    "isync"                            ; mbsync
    "mu"                               ; Maildir utils, includes mu4e
    )))

;; i3 window manager and related programs
(define-public i3
  (list
   "i3-wm"
   "dmenu"
   "network-manager-applet"
   "xfce4-panel"
   "xsetroot"))

(define-public xfce
  (list
   "xfce4"
   "xfce4-whiskermenu-plugin"
   "network-manager-applet"))

(define-public browser
  (list
   "icecat"
   "firefox-pretender"))

(define-public stumpwm
  (list
   "sbcl-stumpwm-ttf-fonts"
   "stumpwm-with-slynk"))

;;; Texlive packages

;; Texlive packages tweaked to handle Emacs org-export, pandoc latex output
;; and the swedish language.

;; Separate texlive packages (like this manifest) are a bit broken at the
;; moment, you should probably use "texlive" instead (the complete
;; distribution).
(define-public texlive
  (list
;;; Latex
   "texlive-scheme-basic"
   "texlive-babel-swedish"
;;; Latex for pandoc
   "texlive-amsfonts"
   "texlive-amsmath"
   "texlive-babel"
   "texlive-booktabs"
   "texlive-iftex"
   "texlive-hyperref"
   "texlive-fancyvrb"
   "texlive-geometry"
   "texlive-graphics"
   "texlive-listings"
   "texlive-lm"
   "texlive-setspace"
   "texlive-tools"
   "texlive-ulem"
   "texlive-unicode-math"
   "texlive-xcolor"
;;; Latex for emacs-org-export
   "texlive-capt-of"
   "texlive-ec"
   "texlive-grfext"
   "texlive-hyperref"
   "texlive-graphics"
   "texlive-microtype"
   "texlive-ulem"
   "texlive-wrapfig"))

(define-public virtualization
  (list
   "qemu"
   "libvirt"
   "virt-manager"))

(define-public lxqt
  (list
   "lxqt"
   "i3-wm"
   "dmenu"
   "network-manager-applet"
   "xfce4-screensaver"
   "xsetroot"))

;; xmonad window manager and related programs
;; (define-public xmonad
;;   (concatenate-manifests
;;    (list (specifications->manifest
;;           (list "ghc-xmonad-contrib"
;;                 "ghc"
;;                 "xmonad"
;;                 "xmobar"
;;                 "xsetroot"))
;;          (package->development-manifest
;;           (specification->package
;;            "xmonad")))))
