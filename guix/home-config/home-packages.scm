(add-to-load-path (string-concatenate (list (getenv "DOTFILE_DIR") "/guix")))
(define-module (home-config home-packages)
  #:use-module (guix profiles)
  #:use-module (gnu packages)
  #:use-module ((collections) #:prefix colls:))

(specifications->manifest
 (append
  colls:base
  colls:i3
  colls:lxqt
  '("texlive")))
