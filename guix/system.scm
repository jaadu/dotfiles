;; This is an operating system configuration template
;; for a "desktop" setup.

(define-module (jaadu desktop)
  #:use-module (gnu)
  #:use-module (gnu packages)
  #:use-module (srfi srfi-1) ; For 'remove'
  #:use-module (gnu services desktop)
  #:use-module (gnu services networking)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services xorg))

(define %hostname "gnurpelfjopp")
(define %username "einar")

(operating-system
  (host-name %hostname)
  (timezone "Europe/Stockholm")
  ;(locale "sv_SE.utf8")	; Swedish seems to break guix
  (keyboard-layout (keyboard-layout "se" "nodeadkeys"
				    #:options (list "ctrl:nocaps"))) ; Caps-lock -> Ctrl

  (kernel-arguments (list "amd_iommu=on"
                          ;; "iommu=pt"
                          ))

 (initrd-modules
  (cons*
   "kvm"                         ; Improves virtualization performance, don't use
   "vfio-pci" "vfio" "vfio_iommu_type1" "vfio_virqfd" ; iommu bypass
   %base-initrd-modules))

  ;; Use the UEFI variant of GRUB with the EFI System
  ;; Partition mounted on /boot/efi.
  (bootloader (bootloader-configuration
		(bootloader grub-efi-bootloader)
		(targets '("/boot/efi"))
		(keyboard-layout keyboard-layout)
		(timeout 5)))

  (swap-devices (list "/dev/sda2"))

  ;; Assume the target root file system is labelled "my-root",
  ;; and the EFI System Partition has UUID 1234-ABCD.
  (file-systems (append
		  (list (file-system
                          (device (uuid "e0a483be-51ab-4e6a-bba5-45ba2348a4c7")
                            ;;(file-system-label "guix-system")
                          )
                          (mount-point "/")
                          (type "ext4"))
			(file-system
			  (device "/dev/sda1")
			  (mount-point "/boot/efi")
			  (type "vfat")))
		  %base-file-systems))

  (users (cons (user-account
		 (name %username)
		 (group "users")
		 (create-home-directory? #f)
		 (supplementary-groups '("wheel"    ; Sudo
					 "netdev"   ; Network devices?
					 "audio"    ;
                                         "libvirt"  ; Virtualization stuff
                                         "kvm"      ; Kernel-based Virtual Machine
					 "video"))) ; Change resolution
	       %base-user-accounts))

  ;; Add a bunch of window managers; we can choose one at
  ;; the log-in screen with F1.
  (packages (append (map specification->package
                         '("openssh"
                           "git"
                           "xorg-server"
                           "xfce"         ; desktop environment
                           "vim-full" "emacs"
                           "pulseaudio" "pavucontrol"
                           "file"
                           "fontconfig" "font-ghostscript" "font-dejavu" "font-gnu-freefont"
                           "nss-certs")) ; for HTTPS access
		    %base-packages))

  ;; Use the "desktop" services, which include the X11
  ;; log-in service, networking with NetworkManager, and more.
  ;;
  ;; Autologin to the desktop must be set at '~/.xsession'.
  (services (cons*
              ;; (simple-service 'etc-modprobe-service etc-service-type
              ;;                 `(("modprobe.d/vfio.conf" ,(plain-file "vfio.conf" "options vfio-pci ids=10de:13c2,10de:0fbb"))))
              (service xfce-desktop-service-type)
              (service slim-service-type
                (slim-configuration
                  (auto-login? #t)
                  (default-user %username)
                  (display ":0")
                  (vt "vt7")
                  (xorg-configuration (xorg-configuration
                                        (keyboard-layout keyboard-layout)))))

              (service libvirt-service-type
                (libvirt-configuration
                  (unix-sock-group "libvirt")))
              (service virtlog-service-type)
              (remove (lambda (service)
                        (eq? (service-kind service) gdm-service-type))
                %desktop-services)))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))


