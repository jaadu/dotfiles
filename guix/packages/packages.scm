(define-module (packages)
  #:use-module (guix build-system copy)
  #:use-module (gnu packages base)
  #:use-module (gnu packages gnuzilla)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
  #:use-module (gnu packages gawk))

(define-public glibc-swedish-utf8-locale
  (make-glibc-utf8-locales
   glibc
   #:locales (list "sv_SE")
   #:name "glibc-swedish-utf8-locale"))

;; (define-public firefox-pretender
;;   (package
;;    (name "firefox-pretender")
;;    (version "1")
;;    (synopsis "A symlink to Icecat")
;;    (input icecat)
;;    (build-system copy-build-system)
;;    (source)
;;    (description "A symlink to Icecat")
;;    (licence)
;;    (home-page)
;;    ))

;; (define-public firefox-pretender
;;   (let ((hello (program-file "hello" #~(display "Hi!"))))
;;     (manifest-entry
;;       (name "foo")
;;       (version "42")
;;       (item
;;        (computed-file "hello-directory"
;;                        #~(let ((bin (string-append #$output "/bin")))
;;                            (mkdir #$output) (mkdir bin)
;;                             (symlink #$hello
;;                                      (string-append bin "/hello"))))))))

;; (define-public hello-there
;;        (package
;;          (name "hello-there")
;;          (version "2.10")
;;          (source (origin
;;                    (method url-fetch)
;;                    (uri (string-append "mirror://gnu/hello/hello-" version
;;                                        ".tar.gz"))
;;                    (sha256
;;                     (base32
;;                      "0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i"))))
;;          (build-system gnu-build-system)
;;          (arguments '(#:configure-flags '("--enable-silent-rules")))
;;          (inputs (list gawk))
;;          (synopsis "Hello, GNU world: An example GNU package")
;;          (description "Guess what GNU Hello prints!")
;;          (home-page "https://www.gnu.org/software/hello/")
;;          (license gpl3+)))
